<?php

declare(strict_types=1);

use App\Orchid\Screens\Event\Event\EventEditScreen;
use App\Orchid\Screens\Event\Event\EventListScreen;
use App\Orchid\Screens\Event\Result\ResultEditScreen;
use App\Orchid\Screens\Event\Result\ResultListScreen;
use App\Orchid\Screens\Member\Agent\AgentListScreen;
use App\Orchid\Screens\Member\Agent\AgentEditScreen;
use App\Orchid\Screens\Member\Arbiter\ArbiterEditScreen;
use App\Orchid\Screens\Member\Arbiter\ArbiterListScreen;
use App\Orchid\Screens\Member\Arbiter\Category\ArbiterCategoryListScreen;
use App\Orchid\Screens\Member\Arbiter\Category\ArbiterCategoryEditScreen;
use App\Orchid\Screens\Member\Athlete\AthleteEditScreen;
use App\Orchid\Screens\Member\Athlete\AthleteListScreen;
use App\Orchid\Screens\Member\Athlete\RegionTeam\RegionTeamEditScreen;
use App\Orchid\Screens\Member\Athlete\RegionTeam\RegionTeamListScreen;
use App\Orchid\Screens\Member\Coach\Category\CoachCategoryEditScreen;
use App\Orchid\Screens\Member\Coach\Category\CoachCategoryListScreen;
use App\Orchid\Screens\Member\Coach\CoachEditScreen;
use App\Orchid\Screens\Member\Coach\CoachListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\Settings\Activity\ActivityListScreen;
use App\Orchid\Screens\Settings\Activity\ActivityEditScreen;
use App\Orchid\Screens\Settings\ActivityClassification\ActivityClassificationEditScreen;
use App\Orchid\Screens\Settings\ActivityClassification\ActivityClassificationListScreen;
use App\Orchid\Screens\Settings\ActivityClassification\Criterion\ActivityClassificationCriterionEditScreen;
use App\Orchid\Screens\Settings\ActivityClassification\Criterion\ActivityClassificationCriterionListScreen;
use App\Orchid\Screens\Settings\City\CityListScreen;
use App\Orchid\Screens\Settings\City\CityEditScreen;
use App\Orchid\Screens\Settings\Organization\OrganizationEditScreen;
use App\Orchid\Screens\Settings\Organization\OrganizationListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Настройки
Route::screen('activities', ActivityListScreen::class)
    ->name('platform.settings.activities')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Дисциплины', route('platform.settings.activities'));
    });

Route::screen('activities/create', ActivityEditScreen::class)
    ->name('platform.settings.activities.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Дисциплины', route('platform.settings.activities.create'));
    });

Route::screen('activities/{activity}/edit', ActivityEditScreen::class)
    ->name('platform.settings.activities.edit')
    ->breadcrumbs(function (Trail $trail, $activity) {
        return $trail
            ->parent('platform.index')
            ->push('Дисциплины', route('platform.settings.activities.edit', $activity));
    });

Route::screen('cities', CityListScreen::class)
    ->name('platform.settings.cities')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Города', route('platform.settings.cities'));
    });

Route::screen('cities/create', CityEditScreen::class)
    ->name('platform.settings.cities.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Города', route('platform.settings.cities.create'));
    });

Route::screen('cities/{city}/edit', CityEditScreen::class)
    ->name('platform.settings.cities.edit')
    ->breadcrumbs(function (Trail $trail, $city) {
        return $trail
            ->parent('platform.index')
            ->push('Города', route('platform.settings.cities.edit', $city));
    });

Route::screen('activityClassificationCriterion', ActivityClassificationCriterionListScreen::class)
    ->name('platform.settings.activity.classification.criteria')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('критерии спортивных классификаций', route('platform.settings.activity.classification.criteria'));
    });

Route::screen('activityClassificationCriterion/create', ActivityClassificationCriterionEditScreen::class)
    ->name('platform.settings.activity.classification.criteria.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('критерии спортивных классификаций', route('platform.settings.activity.classification.criteria.create'));
    });

Route::screen('activityClassificationCriterion/{activityClassificationCriterion}/edit', ActivityClassificationCriterionEditScreen::class)
    ->name('platform.settings.activity.classification.criteria.edit')
    ->breadcrumbs(function (Trail $trail, $activityClassificationCriterion) {
        return $trail
            ->parent('platform.index')
            ->push('критерии спортивных классификаций', route('platform.settings.activity.classification.criteria.edit', $activityClassificationCriterion));
    });

Route::screen('activityClassification', ActivityClassificationListScreen::class)
    ->name('platform.settings.activity.classifications')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('спортивные классификации', route('platform.settings.activity.classifications'));
    });

Route::screen('activityClassification/create', ActivityClassificationEditScreen::class)
    ->name('platform.settings.activity.classification.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('спортивные классификации', route('platform.settings.activity.classification.create'));
    });

Route::screen('activityClassification/{activityClassification}/edit', ActivityClassificationEditScreen::class)
    ->name('platform.settings.activity.classification.edit')
    ->breadcrumbs(function (Trail $trail, $activityClassification) {
        return $trail
            ->parent('platform.index')
            ->push('спортивные классификации', route('platform.settings.activity.classification.edit', $activityClassification));
    });

Route::screen('organizations', OrganizationListScreen::class)
    ->name('platform.settings.organizations')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Организации', route('platform.settings.organizations'));
    });

Route::screen('organization/create', OrganizationEditScreen::class)
    ->name('platform.settings.organization.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Организации', route('platform.settings.organization.create'));
    });

Route::screen('organization/{organization}/edit', OrganizationEditScreen::class)
    ->name('platform.settings.organization.edit')
    ->breadcrumbs(function (Trail $trail, $organization) {
        return $trail
            ->parent('platform.index')
            ->push('Организации', route('platform.settings.organization.edit', $organization));
    });

// Участники

Route::screen('agents', AgentListScreen::class)
    ->name('platform.member.agents')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Агенты', route('platform.member.agents'));
    });

Route::screen('agent/create', AgentEditScreen::class)
    ->name('platform.member.agent.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Агенты', route('platform.member.agent.create'));
    });

Route::screen('agent/{agent}/edit', AgentEditScreen::class)
    ->name('platform.member.agent.edit')
    ->breadcrumbs(function (Trail $trail, $agent) {
        return $trail
            ->parent('platform.index')
            ->push('Агенты', route('platform.member.agent.edit', $agent));
    });

Route::screen('arbiters', ArbiterListScreen::class)
    ->name('platform.member.arbiters')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Судьи', route('platform.member.arbiters'));
    });

Route::screen('arbiters/create', ArbiterEditScreen::class)
    ->name('platform.member.arbiter.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Судьи', route('platform.member.arbiter.create'));
    });

Route::screen('arbiter/{arbiter}/edit', ArbiterEditScreen::class)
    ->name('platform.member.arbiter.edit')
    ->breadcrumbs(function (Trail $trail, $arbiter) {
        return $trail
            ->parent('platform.index')
            ->push('Судьи', route('platform.member.arbiter.edit', $arbiter));
    });

Route::screen('arbiter/categories', ArbiterCategoryListScreen::class)
    ->name('platform.member.arbiter.categories')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Судейские категории', route('platform.member.arbiter.categories'));
    });

Route::screen('arbiter/categories/create', ArbiterCategoryEditScreen::class)
    ->name('platform.member.arbiter.category.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Судейские категории', route('platform.member.arbiter.category.create'));
    });

Route::screen('arbiter/categories/{arbiterCategory}/edit', ArbiterCategoryEditScreen::class)
    ->name('platform.member.arbiter.category.edit')
    ->breadcrumbs(function (Trail $trail, $arbiterCategory) {
        return $trail
            ->parent('platform.index')
            ->push('Судейские категории', route('platform.member.arbiter.category.edit', $arbiterCategory));
    });

Route::screen('coaches', CoachListScreen::class)
    ->name('platform.member.coaches')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Тренеры', route('platform.member.coaches'));
    });

Route::screen('coach/create', CoachEditScreen::class)
    ->name('platform.member.coach.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Тренеры', route('platform.member.coach.create'));
    });

Route::screen('coach/{coach}/edit', CoachEditScreen::class)
    ->name('platform.member.coach.edit')
    ->breadcrumbs(function (Trail $trail, $coach) {
        return $trail
            ->parent('platform.index')
            ->push('Тренеры', route('platform.member.coach.edit', $coach));
    });

Route::screen('coach/categories', CoachCategoryListScreen::class)
    ->name('platform.member.coach.categories')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Тренерские категории', route('platform.member.coach.categories'));
    });

Route::screen('coach/categories/create', CoachCategoryEditScreen::class)
    ->name('platform.member.coach.category.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Тренерские категории', route('platform.member.coach.category.create'));
    });

Route::screen('coach/categories/{coachCategory}/edit', CoachCategoryEditScreen::class)
    ->name('platform.member.coach.category.edit')
    ->breadcrumbs(function (Trail $trail, $coachCategory) {
        return $trail
            ->parent('platform.index')
            ->push('Тренерские категории', route('platform.member.coach.category.edit', $coachCategory));
    });

Route::screen('athletes', AthleteListScreen::class)
    ->name('platform.member.athletes')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Атлеты', route('platform.member.athletes'));
    });

Route::screen('athlete/create', AthleteEditScreen::class)
    ->name('platform.member.athlete.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Атлеты', route('platform.member.athlete.create'));
    });

Route::screen('athlete/{athlete}/edit', AthleteEditScreen::class)
    ->name('platform.member.athlete.edit')
    ->breadcrumbs(function (Trail $trail, $athlete) {
        return $trail
            ->parent('platform.index')
            ->push('Атлеты', route('platform.member.athlete.edit', $athlete));
    });

Route::screen('athlete/regionTeams', RegionTeamListScreen::class)
    ->name('platform.member.athlete.regionTeams')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Областные сборные', route('platform.member.athlete.regionTeams'));
    });

Route::screen('athlete/regionTeams/create', RegionTeamEditScreen::class)
    ->name('platform.member.athlete.regionTeam.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Областные сборные', route('platform.member.athlete.regionTeam.create'));
    });

Route::screen('athlete/regionTeams/{regionTeam}/edit', RegionTeamEditScreen::class)
    ->name('platform.member.athlete.regionTeam.edit')
    ->breadcrumbs(function (Trail $trail, $regionTeam) {
        return $trail
            ->parent('platform.index')
            ->push('Областные сборные', route('platform.member.athlete.regionTeam.edit', $regionTeam));
    });

// события

Route::screen('events', EventListScreen::class)
    ->name('platform.event.events')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('События', route('platform.event.events'));
    });

Route::screen('event/create', EventEditScreen::class)
    ->name('platform.event.event.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('События', route('platform.event.event.create'));
    });

Route::screen('event/{event}/edit', EventEditScreen::class)
    ->name('platform.event.event.edit')
    ->breadcrumbs(function (Trail $trail, $event) {
        return $trail
            ->parent('platform.index')
            ->push('События', route('platform.event.event.edit', $event));
    });

Route::screen('results', ResultListScreen::class)
    ->name('platform.event.results')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Результаты', route('platform.event.events'));
    });

Route::screen('result/create', ResultEditScreen::class)
    ->name('platform.event.result.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Результаты', route('platform.event.event.create'));
    });

Route::screen('result/{result}/edit', ResultEditScreen::class)
    ->name('platform.event.result.edit')
    ->breadcrumbs(function (Trail $trail, $result) {
        return $trail
            ->parent('platform.index')
            ->push('Результаты', route('platform.event.result.edit', $result));
    });
