<?php

return [
    'seeds' => [
        'dumb_db_name' => 'tag_dumb'
    ],
    'image_types' => [
        'organization' => 'organization',
        'coach' => 'coach',
        'athlete' => 'athlete',
        'athlete_insurance' => 'athlete_insurance',
        'athlete_rusada' => 'athlete_rusada',
        'coach_license' => 'coach_license'
    ]
];
