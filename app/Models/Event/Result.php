<?php

namespace App\Models\Event;

use App\Models\Activity;
use App\Models\ActivityClassification;
use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property int $athlete_id
 * @property int $coach_id
 * @property int $activity_id
 * @property int $event_id
 * @property int $result_hours
 * @property int $minute
 * @property int $result_minutes
 * @property int $result_seconds
 * @property int $result_milliseconds
 * @property int $result_time_in_milliseconds
 * @property string $result_in_meters
 * @property int $points
 * @property int $athlete_place
 * @property Activity|null $activity
 * @property Athlete|null $athlete
 * @property Coach|null $coach
 * @property Event|null $event
 * @property string $time
 */
class Result extends Model
{
    use Filterable, HasFactory, AsSource, Chartable;

    protected $fillable = [
        'athlete_id',
        'coach_id',
        'activity_id',
        'event_id',
        'athlete_place',
        'result_hours',
        'result_minutes',
        'result_seconds',
        'result_milliseconds',
        'result_time_in_milliseconds',
        'result_in_meters',
        'points',
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'athlete_id',
        'coach_id',
        'event_id',
        'activity_id',
        'athlete_place',
        'result_in_meters',
        'points',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'athlete_id',
        'coach_id',
        'event_id',
        'activity_id',
        'athlete_place',
        'result_in_meters',
        'points',
    ];

    public function activity(): hasOne
    {
        return $this->hasOne(Activity::class, 'id', 'activity_id');
    }

    public function coach(): hasOne
    {
        return $this->hasOne(Coach::class, 'id', 'coach_id');
    }

    public function athlete(): hasOne
    {
        return $this->hasOne(Athlete::class, 'id', 'athlete_id');
    }

    public function event(): hasOne
    {
        return $this->hasOne(Event::class, 'id', 'event');
    }

    protected function time(): Attribute
    {
        $millisecond = $this->result_time_in_milliseconds % 1000;

        $time = ($this->result_time_in_milliseconds !== null)
            ? Carbon::parse($this->result_time_in_milliseconds / 1000)->format('H:i:s'). (($millisecond != 0) ? '.'.($millisecond / 10) : '')
            : '-';

        return Attribute::make(
            get: fn ($value) => $time
        );
    }
}
