<?php

namespace App\Models\Event;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $date_start
 * @property string $date_end
 * @property int $city_id
 * @property string $name
 * @property string $place_name
 * @property City|null $city
 */
class Event extends Model
{
    use Filterable, HasFactory, AsSource, Chartable;

    protected $fillable = [
        'date_start' ,
        'date_end',
        'city_id',
        'name',
        'place_name'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'date_start',
        'date_end',
        'city_id',
        'name',
        'place_name',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'date_start',
        'date_end',
        'city_id',
        'name',
        'place_name',
    ];

    public function city(): HasOne
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
