<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $site_address
 * @property string $image_url
 * @property string $director_fio
 * @property int $city_id
 * @property City $city
 */
class Organization extends Model
{
    use  Filterable,HasFactory, AsSource, Chartable;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'site_address',
        'image_url',
        'director_fio',
        'city_id',
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'phone',
        'email',
        'address',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'phone',
        'email',
        'address',
    ];

    public function city(): HasOne
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
