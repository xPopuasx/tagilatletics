<?php

namespace App\Models;

use App\Models\Event\Result;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $name
 * @property array $findClassification
 */
class Activity extends Model
{
    use  Filterable,HasFactory, AsSource, Chartable;

    protected $fillable  = [
        'name'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'name',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'name',
    ];

    public function result(): hasOne
    {
        return $this->hasOne(Result::class);
    }

    protected function findClassification(): Attribute
    {
        $ActivityClassification = ActivityClassification::query()->where('name', $this->name)
            ->get()->load('criterion')->toArray();

        return Attribute::make(
            get: fn ($value) => $ActivityClassification
        );
    }
}
