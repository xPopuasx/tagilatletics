<?php

namespace App\Models\Member;

use App\Traits\Model\ModelHasOrganizations;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $patronymic
 * @property string $email
 * @property string $phone
 * @property string $birth_date
 * @property int $coach_category_id
 * @property Collection|null $organizations
 * @property string $fio
 * @property string $image_url
 * @property string $rank
 * @property string $license_image_url
 * @property CoachCategory|null $category
 */
class Coach extends Model
{
    use Filterable, HasFactory, AsSource, Chartable, ModelHasOrganizations;

    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'rank',
        'birth_date',
        'coach_category_id',
        'phone',
        'image_url',
        'license_image_url'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'first_name',
        'last_name',
        'birth_date',
        'patronymic',
        'email',
        'phone',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'first_name',
        'last_name',
        'birth_date',
        'patronymic',
        'email',
        'phone',
    ];

    protected function fio(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->first_name . ' ' . $this->last_name . ' ' . $this->patronymic,
        );
    }

    public function category(): hasOne
    {
        return $this->hasOne(CoachCategory::class, 'id', 'coach_category_id');
    }
}
