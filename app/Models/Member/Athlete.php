<?php

namespace App\Models\Member;

use App\Models\Event\Result;
use App\Traits\Model\ModelHasActivity;
use App\Traits\Model\ModelHasCoaches;
use App\Traits\Model\ModelHasOrganizations;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;


/**
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $patronymic
 * @property string $birth_date
 * @property boolean $umo
 * @property string $fio
 * @property string $image_url
 * @property string $gender
 * @property int $region_team_id
 * @property string $qualification
 * @property Collection|null $organizations
 * @property Collection|null $activities
 * @property Collection|null $coaches
 * @property RegionTeam|null $regionTeam
 * @property Collection|null $results
 */
class Athlete extends Model
{
    use Filterable, HasFactory, AsSource, Chartable, ModelHasCoaches, ModelHasActivity, ModelHasOrganizations, Attachable;

    protected $fillable = [
        'last_name',
        'first_name',
        'patronymic',
        'birth_date',
        'qualification',
        'gender',
        'umo',
        'image_url',
        'region_team_id',
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'first_name',
        'birth_date',
        'gender',
        'region_team_id',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'first_name',
        'birth_date',
        'gender',
        'region_team_id',
    ];

    public function regionTeam(): hasOne
    {
        return $this->hasOne(RegionTeam::class ,'id', 'region_team_id');
    }

    public function results(): hasMany
    {
        return $this->hasMany(Result::class);
    }

    protected function fio(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->first_name . ' ' . $this->last_name . ' ' . $this->patronymic,
        );
    }
}
