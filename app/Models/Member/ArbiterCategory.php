<?php

namespace App\Models\Member;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $name
 */
class ArbiterCategory extends Model
{
    use Filterable, HasFactory, AsSource, Chartable;

    protected $fillable =[
        'name'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'name',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'name',
    ];
}
