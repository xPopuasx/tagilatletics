<?php

namespace App\Models\Member;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $region_name
 * @property int $age_up_to
 * @property Collection|null $athletes
 */
class RegionTeam extends Model
{
    use Filterable, HasFactory, AsSource, Chartable;

    protected $fillable = [
        'region_name',
        'age_up_to'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'region_name',
        'age_up_to',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'region_name',
        'age_up_to',
    ];

    public function athletes(): HasMany
    {
        return $this->hasMany(Athlete::class);
    }
}
