<?php

namespace App\Models\Member;

use App\Traits\Model\ModelHasOrganizations;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $patronymic
 * @property string $email
 * @property string $phone
 * @property int $arbiter_category_id
 * @property Collection|null $organizations
 * @property Carbon $date_start
 * @property Carbon $date_end
 * @property string $qualification_order
 * @property string $fio
 * @property ArbiterCategory|null $category
 */
class Arbiter extends Model
{
    use Filterable, HasFactory, AsSource, Chartable, ModelHasOrganizations;

    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'arbiter_category_id',
        'phone',
        'date_start',
        'date_end',
        'qualification_order',
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'phone',
        'date_start',
        'date_end',
        'qualification_order',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'phone',
        'date_start',
        'date_end',
        'qualification_order',
    ];

    protected function fio(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->first_name . ' ' . $this->last_name . ' ' . $this->patronymic,
        );
    }

    public function category(): hasOne
    {
        return $this->hasOne(ArbiterCategory::class, 'id', 'arbiter_category_id');
    }
}
