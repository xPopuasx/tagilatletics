<?php

namespace App\Models\member;

use App\Traits\Model\ModelHasOrganizations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $patronymic
 * @property string $email
 * @property string $phone
 * @property Collection|null $organizations
 */
class Agent extends Model
{
    use Filterable, HasFactory, AsSource, Chartable, ModelHasOrganizations;

    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'phone',
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'phone',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'phone',
    ];
}
