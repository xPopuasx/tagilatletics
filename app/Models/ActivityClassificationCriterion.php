<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $name
 * @property Collection|null $activityClassification
 */
class ActivityClassificationCriterion extends Model
{
    use  Filterable,HasFactory, AsSource, Chartable;

    protected $fillable =[
        'name'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'name',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'name',
    ];

    public function activityClassification(): hasMany
    {
        return $this->hasMany(ActivityClassification::class);
    }
}
