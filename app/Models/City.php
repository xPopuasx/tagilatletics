<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;


/**
 * @property int $id
 * @property int $name
 * @property int $region_name
 * @property int $county
 */
class City extends Model
{
    use  Filterable,HasFactory, AsSource, Chartable;

    protected $fillable = [
        'name',
        'region_name',
        'county'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'name',
        'region_name',
        'county'
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'name',
        'region_name',
        'county'
    ];
}
