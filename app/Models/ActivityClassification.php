<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int $id
 * @property string $name
 * @property string $unit
 * @property int $criterion_id
 * @property float $discharge_msmk
 * @property float $discharge_ms
 * @property float $discharge_kms
 * @property float $discharge_first_adult
 * @property float $discharge_second_adult
 * @property float $discharge_third_adult
 * @property float $discharge_first_youthful
 * @property float $discharge_second_youthful
 * @property float $discharge_third_youthful
 * @property ActivityClassificationCriterion|null $criterion
 */
class ActivityClassification extends Model
{
    use  Filterable,HasFactory, AsSource, Chartable;

    protected $fillable = [
        'name',
        'unit',
        'criterion_id',
        'discharge_msmk',
        'discharge_ms',
        'discharge_kms',
        'discharge_first_adult',
        'discharge_second_adult',
        'discharge_third_adult',
        'discharge_first_youthful',
        'discharge_second_youthful',
        'discharge_third_youthful'
    ];

    /**
     * @var array
     */
    protected array $allowedFilters = [
        'id',
        'name',
    ];

    /**
     * @var array
     */
    protected array $allowedSorts = [
        'id',
        'name',
        'unit',
        'criterion_id',
        'discharge_ms',
        'discharge_kms',
        'discharge_msmk',
        'discharge_first_adult',
        'discharge_second_adult',
        'discharge_third_adult',
        'discharge_first_youthful',
        'discharge_second_youthful',
        'discharge_third_youthful'
    ];

    public function criterion(): hasOne
    {
        return $this->hasOne(ActivityClassificationCriterion::class, 'id', 'criterion_id');
    }
}
