<?php

namespace App\Services;

use App\Models\City;
use Illuminate\Database\Eloquent\Model;

class CityService extends Service
{
    /**
     * @param  array  $request
     * @return City|Model
     */
    public function create(array $request): City|Model
    {
        return City::query()->create($request['city']);
    }

    /**
     * @param  City  $model
     * @param  array  $request
     * @return City|Model
     */
    public function update(Model $model, array $request): City|Model
    {
        $model->update($request['city']);

        return $model->fresh();
    }

    /**
     * @param  City $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getCitiesToSelect(): array
    {
        $data =  City::query()->select('id', 'name', 'region_name')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name']. ' (' . $item['region_name'] . ')';
        }

        return $outputData;
    }
}
