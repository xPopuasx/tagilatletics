<?php

namespace App\Services;

use App\Models\Activity;
use App\Models\ActivityClassification;
use Illuminate\Database\Eloquent\Model;

class ActivityClassificationService extends Service
{
    /**
     * @param  array  $request
     * @return ActivityClassification|Model
     */
    public function create(array $request): ActivityClassification|Model
    {
        return ActivityClassification::query()->create($request['activity']);
    }

    /**
     * @param  ActivityClassification  $model
     * @param  array  $request
     * @return ActivityClassification|Model
     */
    public function update(Model $model, array $request): ActivityClassification|Model
    {
        $model->update($request['activity']);

        return $model->fresh();
    }

    /**
     * @param  ActivityClassification $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @param Activity $activity
     * @param string $result
     * @return array|array[]
     */
    public function getActivitiesClassificationToTable(Activity $activity, string $result): array
    {
        if(count($activity->findClassification) == 0){
            return [];
        }

        $data['name'] = $activity->name;
        $data['result'] = $result;

        $i = 0;
        foreach ($activity->findClassification as $result){
            $data['items'][$i]['criterion'] = '<div>'.($result['criterion']['name'] ?? 'Критерий не указан'). '</div> <small class="text-muted">Ед. измерения: ' .($result['unit'].'.' ?? 'ед. изм. не казана'). '</small>';
            $data['items'][$i]['discharge_msmk'] = $result['discharge_msmk'] ?? '-';
            $data['items'][$i]['discharge_ms'] = $result['discharge_ms'] ?? '-';
            $data['items'][$i]['discharge_kms'] = $result['discharge_kms'] ?? '-';
            $data['items'][$i]['discharge_first_adult'] = $result['discharge_first_adult'] ?? '-';
            $data['items'][$i]['discharge_second_adult'] = $result['discharge_second_adult'] ?? '-';
            $data['items'][$i]['discharge_third_adult'] = $result['discharge_third_adult'] ?? '-';
            $data['items'][$i]['discharge_first_youthful'] = $result['discharge_first_youthful'] ?? '-';
            $data['items'][$i]['discharge_second_youthful'] = $result['discharge_second_youthful'] ?? '-';
            $data['items'][$i]['discharge_third_youthful'] = $result['discharge_third_youthful'] ?? '-';
            $i++;
        }

        return ['data' => $data];
    }
}
