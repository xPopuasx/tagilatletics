<?php

namespace App\Services;

use App\Models\Activity;
use App\Models\ActivityClassificationCriterion;
use Illuminate\Database\Eloquent\Model;

class ActivityClassificationCriterionService extends Service
{
    /**
     * @param  array  $request
     * @return ActivityClassificationCriterion|Model
     */
    public function create(array $request): ActivityClassificationCriterion|Model
    {
        return ActivityClassificationCriterion::query()->create($request['activityClassificationCriterion']);
    }

    /**
     * @param  ActivityClassificationCriterion  $model
     * @param  array  $request
     * @return ActivityClassificationCriterion|Model
     */
    public function update(Model $model, array $request): ActivityClassificationCriterion|Model
    {
        $model->update($request['activityClassificationCriterion']);

        return $model->fresh();
    }

    /**
     * @param  ActivityClassificationCriterion $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getCriteriaToSelect(): array
    {
        $data =  ActivityClassificationCriterion::query()->select('id', 'name')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name'];
        }

        return $outputData;
    }
}
