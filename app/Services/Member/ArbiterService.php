<?php

namespace App\Services\Member;

use App\Models\City;
use App\Models\member\Agent;
use App\Models\Member\Arbiter;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ArbiterService extends Service
{
    /**
     * @param  array  $request
     * @return Arbiter|Model
     */
    public function create(array $request): Model|Arbiter
    {
        return DB::transaction(function () use ($request){
            if($request['arbiter']['arbiter_category_id'] == 'no_value'){
                unset($request['arbiter']['arbiter_category_id']);
            }
            /** @var Arbiter $model */
            $model = Arbiter::query()->create($request['arbiter']);

            $model->organizations()->sync($request['arbiter']['organizations'] ?? []);

            return $model->fresh();
        });
    }

    /**
     * @param  Arbiter  $model
     * @param  array  $request
     * @return Model|Arbiter
     */
    public function update(Model $model, array $request): Model|Arbiter
    {
        return DB::transaction(function () use ($model, $request){
            if($request['arbiter']['arbiter_category_id'] == 'no_value'){
                unset($request['arbiter']['arbiter_category_id']);
            }

            $model->update($request['arbiter']);

            $model->organizations()->sync($request['arbiter']['organizations'] ?? []);

            return $model->fresh();
        });
    }

    /**
     * @param  Arbiter $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
}
