<?php

namespace App\Services\Member;

use App\Models\Member\ArbiterCategory;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

class ArbiterCategoryService extends Service
{
    /**
     * @param  array  $request
     * @return ArbiterCategory|Model
     */
    public function create(array $request): ArbiterCategory|Model
    {
        return ArbiterCategory::query()->create($request['arbiterCategory']);
    }

    /**
     * @param  ArbiterCategory  $model
     * @param  array  $request
     * @return ArbiterCategory|Model
     */
    public function update(Model $model, array $request): ArbiterCategory|Model
    {
        $model->update($request['arbiterCategory']);

        return $model->fresh();
    }

    /**
     * @param  ArbiterCategory $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getArbiterCategoriesToSelect(): array
    {
        $data =  ArbiterCategory::query()->select('id', 'name')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name'];
        }

        return $outputData;
    }
}
