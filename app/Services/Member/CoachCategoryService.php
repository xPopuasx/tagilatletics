<?php

namespace App\Services\Member;

use App\Models\Member\ArbiterCategory;
use App\Models\Member\CoachCategory;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

class CoachCategoryService extends Service
{
    /**
     * @param  array  $request
     * @return CoachCategory|Model
     */
    public function create(array $request): CoachCategory|Model
    {
        return CoachCategory::query()->create($request['coachCategory']);
    }

    /**
     * @param  CoachCategory  $model
     * @param  array  $request
     * @return CoachCategory|Model
     */
    public function update(Model $model, array $request): CoachCategory|Model
    {
        $model->update($request['coachCategory']);

        return $model->fresh();
    }

    /**
     * @param  CoachCategory $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getCoachCategoriesToSelect(): array
    {
        $data =  CoachCategory::query()->select('id', 'name')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name'];
        }

        return $outputData;
    }
}
