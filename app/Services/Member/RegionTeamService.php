<?php

namespace App\Services\Member;

use App\Models\Member\RegionTeam;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

class RegionTeamService extends Service
{
    /**
     * @param  array  $request
     * @return RegionTeam|Model
     */
    public function create(array $request): RegionTeam|Model
    {
        return RegionTeam::query()->create($request['regionTeam']);
    }

    /**
     * @param  RegionTeam  $model
     * @param  array  $request
     * @return RegionTeam|Model
     */
    public function update(Model $model, array $request): RegionTeam|Model
    {
        $model->update($request['regionTeam']);

        return $model->fresh();
    }

    /**
     * @param  RegionTeam $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getRegionTeamsToSelect(): array
    {
        $data =  RegionTeam::query()->select('id', 'region_name', 'age_up_to')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['region_name']. ' (' . $item['age_up_to'] . ' лет)';
        }

        return $outputData;
    }
}
