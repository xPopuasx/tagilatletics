<?php

namespace App\Services\Member;

use App\Models\City;
use App\Models\Member\Coach;
use App\Models\Member\CoachCategory;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CoachService extends Service
{
    /**
     * @param  array  $request
     * @return Coach|Model
     */
    public function create(array $request): Model|Coach
    {
        return DB::transaction(function () use ($request){
            if($request['coach']['coach_category_id'] == 'no_value'){
                unset($request['coach']['coach_category_id']);
            }

            /** @var Coach $model */
            $model = Coach::query()->create($request['coach']);

            $model->organizations()->sync($request['coach']['organizations'] ?? []);

            return $model->fresh();
        });
    }

    /**
     * @param  Coach  $model
     * @param  array  $request
     * @return City|Coach
     */
    public function update(Model $model, array $request): Model|Coach
    {
        return DB::transaction(function () use ($model, $request){
            if($request['coach']['coach_category_id'] == 'no_value'){
                unset($request['coach']['coach_category_id']);
            }

            $model->update($request['coach']);

            $model->organizations()->sync($request['coach']['organizations'] ?? []);

            return $model->fresh();
        });
    }

    /**
     * @param  Coach $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getCoachesToSelect(): array
    {
        $data = Coach::query()->select('id', 'first_name', 'last_name', 'patronymic', 'rank')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['first_name'] .' '. $item['last_name'] .' '. $item['patronymic'] .' ('. ($item['rank'] ?? 'Звание не указано') .')';
        }

        return $outputData;
    }
}
