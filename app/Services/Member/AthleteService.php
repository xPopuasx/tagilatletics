<?php

namespace App\Services\Member;

use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AthleteService extends Service
{
    /**
     * @param  array  $request
     * @return Athlete|Model
     */
    public function create(array $request): Model|Athlete
    {
        return DB::transaction(function () use ($request){
            if($request['athlete']['region_team_id'] == 'no_value'){
                unset($request['athlete']['region_team_id']);
            }

            /** @var Athlete $model */
            $model = Athlete::query()->create($request['athlete']);

            $this->syncAthleteRelates($model, $request);

            return $model;
        });
    }

    /**
     * @param  Athlete  $model
     * @param  array  $request
     * @return Model|Athlete
     */
    public function update(Model $model, array $request): Model|Athlete
    {
        return DB::transaction(function () use ($model, $request){
            if($request['athlete']['region_team_id'] == 'no_value'){
                unset($request['athlete']['region_team_id']);
            }

            $model->update($request['athlete']);

            $this->syncAthleteRelates($model, $request);

            return $model->fresh();
        });
    }

    /**
     * @param  Athlete $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @param Athlete $model
     * @param array $request
     * @return void
     */
    private function syncAthleteRelates(Athlete $model, array $request): void
    {
        $model->organizations()->sync($request['athlete']['organizations'] ?? []);

        $model->coaches()->sync($request['athlete']['coaches'] ?? []);

        $model->activities()->sync($request['athlete']['activities'] ?? []);

        $model->attachment()->sync($request['athlete']['attachment'] ?? []);
    }

    /**
     * @return array
     */
    public function getAthleteToSelect(): array
    {
        $data = Athlete::query()->select('id', 'first_name', 'last_name', 'patronymic', 'qualification')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['first_name'] .' '. $item['last_name'] .' '. $item['patronymic'] .' ('. ($item['qualification'] ?? 'Разряд не указан') .')';
        }

        return $outputData;
    }
}
