<?php

namespace App\Services\Member;

use App\Models\City;
use App\Models\member\Agent;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AgentService extends Service
{
    /**
     * @param  array  $request
     * @return Agent|Model
     */
    public function create(array $request): Model|Agent
    {
        return DB::transaction(function () use ($request){
            /** @var Agent $model */
            $model = Agent::query()->create($request['agent']);

            $model->organizations()->sync($request['agent']['organizations'] ?? []);

            return $model->fresh();
        });
    }

    /**
     * @param  Agent  $model
     * @param  array  $request
     * @return City|Agent
     */
    public function update(Model $model, array $request): Model|Agent
    {
        return DB::transaction(function () use ($model, $request){

            $model->update($request['agent']);

            $model->organizations()->sync($request['agent']['organizations'] ?? []);

            return $model->fresh();
        });
    }

    /**
     * @param  Agent $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
}
