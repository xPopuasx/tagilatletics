<?php

namespace App\Services\Event;

use App\Models\Event\Result;
use App\Services\Service;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ResultService extends Service
{
    /**
     * @param  array  $request
     * @return Result|Model
     */
    public function create(array $request): Result|Model
    {
        return Result::query()->create($request['result']);
    }

    /**
     * @param  Result  $model
     * @param  array  $request
     * @return Result|Model
     */
    public function update(Model $model, array $request): Result|Model
    {
        if($request['result']['result_in_meters'] == null){
            $time = $request['result']['result_hours']. ':'.
                $request['result']['result_minutes']. ':'.
                $request['result']['result_seconds']. '.'.
                $request['result']['result_milliseconds'];

            $request['result']['result_time_in_milliseconds'] = Carbon::parse($time)->getPreciseTimestamp(3);
        }

        $model->update($request['result']);

        return $model->fresh();
    }

    /**
     * @param  Result $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
}
