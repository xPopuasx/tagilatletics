<?php

namespace App\Services\Event;

use App\Models\Event\Event;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

class EventService extends Service
{
    /**
     * @param  array  $request
     * @return Event|Model
     */
    public function create(array $request): Event|Model
    {
        return Event::query()->create($request['event']);
    }

    /**
     * @param  Event  $model
     * @param  array  $request
     * @return Event|Model
     */
    public function update(Model $model, array $request): Event|Model
    {
        $model->update($request['event']);

        return $model->fresh();
    }

    /**
     * @param  Event $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getEventsToSelect(): array
    {
        $data = Event::query()->select('name', 'id')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name'];
        }

        return $outputData;
    }
}
