<?php

namespace App\Services;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Model;

class OrganizationService extends Service
{
    /**
     * @param  array  $request
     * @return Organization|Model
     */
    public function create(array $request): Organization|Model
    {
        if($request['organization']['city_id'] == 'no_value'){
            unset($request['organization']['city_id']);
        }

        return Organization::query()->create($request['organization']);
    }

    /**
     * @param  Organization  $model
     * @param  array  $request
     * @return Organization|Model
     */
    public function update(Model $model, array $request): Organization|Model
    {
        if($request['organization']['city_id'] == 'no_value'){
            unset($request['organization']['city_id']);
        }

        $model->update($request['organization']);

        return $model->fresh();
    }

    /**
     * @param  Organization $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @return array
     */
    public function getOrganizationsToSelect(): array
    {
        $data =  Organization::query()->select('id', 'name')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name'];
        }

        return $outputData;
    }
}
