<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

abstract class Service
{
    /** Создает модель */
    abstract protected function create(array $request): Model;

    /** Обновляет модель */
    abstract protected function update(Model $model, array $request): Model;

    /** Удлакние модели */
    abstract protected function delete(Model $model): bool;
}
