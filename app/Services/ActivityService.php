<?php

namespace App\Services;

use App\Models\Activity;
use Illuminate\Database\Eloquent\Model;

class ActivityService extends Service
{
    /**
     * @param  array  $request
     * @return Activity|Model
     */
    public function create(array $request): Activity|Model
    {
        return Activity::query()->create($request['activity']);
    }

    /**
     * @param  Activity  $model
     * @param  array  $request
     * @return Activity|Model
     */
    public function update(Model $model, array $request): Activity|Model
    {
        $model->update($request['activity']);

        return $model->fresh();
    }

    /**
     * @param  Activity $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    public function getActivitiesToSelect(): array
    {
        $data = Activity::query()->select('id', 'name')
            ->get()
            ->toArray();

        if(count($data) == 0){
            return [];
        }

        $outputData = [];

        foreach ($data as $item){
            $outputData[$item['id']] = $item['name'];
        }

        return $outputData;
    }

}
