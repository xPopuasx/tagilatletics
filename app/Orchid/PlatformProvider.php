<?php

declare(strict_types=1);

namespace App\Orchid;

use App\Models\User;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            // События
            Menu::make('События')
                ->icon('calendar')
                ->route('platform.event.events')
                ->title('События')
                ->permission('platform.event.events'),

            Menu::make('Результаты')
                ->icon('fa.id-card')
                ->route('platform.event.results')
                ->permission('platform.event.results'),

            // Участники проекта
            Menu::make('Агенты')
                ->icon('friends')
                ->route('platform.member.agents')
                ->title('Участники')
                ->permission('platform.member.agents'),

            Menu::make('Арбитры')
                ->icon('clock')
                ->list([
                    Menu::make('Категории')
                        ->icon('number-list')
                        ->route('platform.member.arbiter.categories')
                        ->permission('platform.member.arbiter.categories'),
                    Menu::make('Арбитры')
                        ->icon('friends')
                        ->route('platform.member.arbiters')
                        ->permission('platform.member.arbiters'),
                ])->permission(['platform.member.arbiters', 'platform.member.arbiter.categories']),

            Menu::make('Тренеры')
                ->icon('badge')
                ->list([
                    Menu::make('Категории')
                        ->icon('number-list')
                        ->route('platform.member.coach.categories')
                        ->permission('platform.member.coach.categories'),
                    Menu::make('Тренера')
                        ->icon('friends')
                        ->route('platform.member.coaches')
                        ->permission('platform.member.coaches'),
                ])->permission(['platform.member.coaches', 'platform.member.coach.categories']),

            Menu::make('Атлеты')
                ->icon('people')
                ->list([
                    Menu::make('Областные сборные')
                        ->icon('share')
                        ->route('platform.member.athlete.regionTeams')
                        ->permission('platform.member.athlete.regionTeams'),
                    Menu::make('Атлеты')
                        ->icon('people')
                        ->route('platform.member.athletes')
                        ->permission('platform.member.athletes'),
                ])
                ->divider()
                ->permission(['platform.member.athletes', 'platform.member.athlete.regionTeams']),

            // Дополнительные нестройки
            Menu::make('Дисциплины')
                ->icon('task')
                ->route('platform.settings.activities')
                ->title('Дополнительные настройки')
                ->permission('platform.settings.activities'),

            Menu::make('Города')
                ->icon('pointer')
                ->route('platform.settings.cities')
                ->permission('platform.settings.cities'),

            Menu::make('Организации')
                ->icon('organization')
                ->route('platform.settings.organizations')
                ->permission('platform.settings.organizations'),

            Menu::make('Спорт. классификации')
                ->icon('book-open')
                ->list([
                    Menu::make('Классификации')
                        ->icon('number-list')
                        ->route('platform.settings.activity.classifications')
                        ->permission('platform.settings.activity.classifications'),
                    Menu::make('Критерии')
                    ->icon('equalizer')
                    ->route('platform.settings.activity.classification.criteria')
                        ->permission('platform.settings.activity.classification.criteria'),
                ])
                ->divider()
                ->permission(['platform.settings.activity.classifications', 'platform.member.athlete.regionTeams']),

            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Access rights')),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('События'))
                ->addPermission('platform.event.events', __('События'))
                ->addPermission('platform.event.results', __('Результаты')),

            ItemPermission::group(__('Участники'))
                ->addPermission('platform.member.agents', __('Агенты'))
                ->addPermission('platform.member.arbiters', __('Арбитры'))
                ->addPermission('platform.member.arbiter.categories', __('Арбитры - категории'))
                ->addPermission('platform.member.coaches', __('Тренеры'))
                ->addPermission('platform.member.coach.categories', __('Тренеры - категории'))
                ->addPermission('platform.member.athletes', __('Атлеты'))
                ->addPermission('platform.member.athlete.regionTeams', __('Областные сборные')),

            ItemPermission::group(__('Дополнительные нестройки'))
                ->addPermission('platform.settings.activities', __('Дисциплины'))
                ->addPermission('platform.settings.cities', __('Города'))
                ->addPermission('platform.settings.organizations', __('Организации'))
                ->addPermission('platform.settings.activity.classifications', __('Спорт. классификации'))
                ->addPermission('platform.settings.activity.classification.criteria', __('Спорт. классификации - Критерии')),

            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
