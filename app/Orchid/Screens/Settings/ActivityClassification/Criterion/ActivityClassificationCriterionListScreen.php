<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Settings\ActivityClassification\Criterion;

use App\Models\ActivityClassificationCriterion;
use App\Orchid\Layouts\Settings\ActivityClassification\Criterion\ActivityClassificationCriterionListLayout;
use Illuminate\Http\Request;
use Orchid\Platform\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ActivityClassificationCriterionListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'activityClassificationCriterion' => ActivityClassificationCriterion::query()
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(10),
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.settings.activity.classification.criteria',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Критерии';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Все установленные критерии в системе';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.settings.activity.classification.criteria.create'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            ActivityClassificationCriterionListLayout::class
        ];
    }

    /**
     * @param Request $request
     * @param User    $user
     */
    public function save(Request $request, User $user): void
    {

    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {

    }
}
