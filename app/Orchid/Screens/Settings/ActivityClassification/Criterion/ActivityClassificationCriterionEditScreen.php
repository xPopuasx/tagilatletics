<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Settings\ActivityClassification\Criterion;

use App\Http\Requests\Settings\Activity\StoreActivityRequest;
use App\Http\Requests\Settings\ActivityClassification\Criterion\StoreActivityClassificationCriterionRequest;
use App\Models\ActivityClassificationCriterion;
use App\Orchid\Layouts\Settings\ActivityClassification\Criterion\ActivityClassificationCriterionEditLayout;
use App\Services\ActivityClassificationCriterionService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ActivityClassificationCriterionEditScreen extends Screen
{
    public ActivityClassificationCriterion $activityClassificationCriterion;

    public function __construct(private ActivityClassificationCriterionService $activityClassificationCriterionService){

    }

    /**
     * Query data.
     *
     * @param ActivityClassificationCriterion $activityClassificationCriterion
     *
     * @return array
     */
    public function query(ActivityClassificationCriterion $activityClassificationCriterion): iterable
    {
        return [
            'activityClassificationCriterion'  => $activityClassificationCriterion,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.settings.activity.classification.criteria',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->activityClassificationCriterion->exists ? 'Редактирование критерия ' : 'Создание критерия';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->activityClassificationCriterion->exists ? 'Редактирование критерия ' : 'Создание критерия';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->activityClassificationCriterion->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                ActivityClassificationCriterionEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. $this->activityClassificationCriterion->exists ? 'отредактировать' : 'создать нову' . ' дисциплину')

        ];
    }

    /**
     * @param ActivityClassificationCriterion $activityClassificationCriterion
     * @param StoreActivityClassificationCriterionRequest $request
     *
     * @return RedirectResponse
     */
    public function save(ActivityClassificationCriterion $activityClassificationCriterion, StoreActivityClassificationCriterionRequest $request): RedirectResponse
    {
        $activityClassificationCriterion->exists
            ? $this->activityClassificationCriterionService->update($activityClassificationCriterion, $request->validated())
            : $this->activityClassificationCriterionService->create($request->validated());

        Toast::info('Запись успешно ' . ($activityClassificationCriterion->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.settings.activity.classification.criteria');
    }

    /**
     * @param ActivityClassificationCriterion $activityClassificationCriterion
     *
     * @return RedirectResponse
     */
    public function remove(ActivityClassificationCriterion $activityClassificationCriterion): RedirectResponse
    {
        $this->activityClassificationCriterionService->delete($activityClassificationCriterion);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.settings.activity.classification.criteria');
    }
}
