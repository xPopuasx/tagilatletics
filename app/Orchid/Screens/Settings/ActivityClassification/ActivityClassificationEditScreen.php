<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Settings\ActivityClassification;

use App\Http\Requests\Settings\Activity\StoreActivityRequest;
use App\Models\ActivityClassification;
use App\Orchid\Layouts\Settings\ActivityClassification\ActivityClassificationEditLayout;
use App\Services\ActivityService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ActivityClassificationEditScreen extends Screen
{
    public ActivityClassification $activityClassification;

    public function __construct(private ActivityService $activityService){

    }

    /**
     * Query data.
     *
     * @param ActivityClassification $activityClassification
     *
     * @return array
     */
    public function query(ActivityClassification $activityClassification): iterable
    {
        return [
            'activityClassification'  => $activityClassification,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.settings.activity.classifications',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->activityClassification->exists ? 'Редактирование классификацию ' : 'Создание классификацию';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->activityClassification->exists ? 'Редактирование классификацию ' : 'Создание классификацию';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->activityClassification->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                ActivityClassificationEditLayout::class,
            ])
                ->title('Классификация')
                ->description('Здесь вы можете '. ($this->activityClassification->exists ? 'отредактировать' : 'создать нову') . ' классификацию')

        ];
    }

    /**
     * @param ActivityClassification $activityClassification
     * @param StoreActivityRequest $request
     *
     * @return RedirectResponse
     */
    public function save(ActivityClassification $activityClassification, StoreActivityRequest $request): RedirectResponse
    {
        $activityClassification->exists
            ? $this->activityService->update($activityClassification, $request->validated())
            : $this->activityService->create($request->validated());

        Toast::info('Запись успешно ' . ($activityClassification->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.settings.activities');
    }

    /**
     * @param ActivityClassification $activityClassification
     *
     * @return RedirectResponse
     */
    public function remove(ActivityClassification $activityClassification): RedirectResponse
    {
        $this->activityService->delete($activityClassification);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.settings.activities');
    }
}
