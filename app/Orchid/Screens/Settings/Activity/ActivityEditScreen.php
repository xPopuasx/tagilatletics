<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Settings\Activity;

use App\Http\Requests\Settings\Activity\StoreActivityRequest;
use App\Models\Activity;
use App\Orchid\Layouts\Settings\Activity\ActivityEditLayout;
use App\Services\ActivityService;
use Illuminate\Http\RedirectResponse;
use Orchid\Access\UserSwitch;
use Orchid\Platform\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ActivityEditScreen extends Screen
{
    public Activity $activity;

    public function __construct(private ActivityService $activityService){

    }

    /**
     * Query data.
     *
     * @param Activity $activity
     *
     * @return array
     */
    public function query(Activity $activity): iterable
    {
        return [
            'activity'  => $activity,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.settings.activities',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->activity->exists ? 'Редактирование дисциплины ' : 'Создание дисциплины';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->activity->exists ? 'Редактирование дисциплины ' : 'Создание дисциплины';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->activity->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                ActivityEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. $this->activity->exists ? 'отредактировать' : 'создать нову' . ' дисциплину')

        ];
    }

    /**
     * @param Activity    $activity
     * @param StoreActivityRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Activity $activity, StoreActivityRequest $request): RedirectResponse
    {
        $activity->exists
            ? $this->activityService->update($activity, $request->validated())
            : $this->activityService->create($request->validated());

        Toast::info('Запись успешно ' . ($activity->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.settings.activities');
    }

    /**
     * @param Activity $activity
     *
     * @return RedirectResponse
     */
    public function remove(Activity $activity): RedirectResponse
    {
        $this->activityService->delete($activity);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.settings.activities');
    }
}
