<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Settings\Organization;

use App\Http\Requests\Settings\Organization\StoreOrganizationRequest;
use App\Models\Organization;
use App\Orchid\Layouts\Settings\Organization\OrganizationEditLayout;
use App\Services\OrganizationService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class OrganizationEditScreen extends Screen
{
    public Organization $organization;

    public function __construct(private OrganizationService $organizationService){

    }

    /**
     * Query data.
     *
     * @param Organization $organization
     *
     * @return array
     */
    public function query(Organization $organization): iterable
    {
        return [
            'organization'  => $organization,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.settings.organizations',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->organization->exists ? 'Редактирование города ' : 'Создание города';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->organization->exists ? 'Редактирование города ' : 'Создание города';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->organization->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                OrganizationEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. $this->organization->exists ? 'отредактировать' : 'создать новую' . ' организацию')

        ];
    }

    /**
     * @param Organization $organization
     * @param StoreOrganizationRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Organization $organization, StoreOrganizationRequest $request): RedirectResponse
    {
        $organization->exists
            ? $this->organizationService->update($organization, $request->validated())
            : $this->organizationService->create($request->validated());

        Toast::info('Запись успешно ' . ($organization->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.settings.organizations');
    }

    /**
     * @param Organization $organization
     *
     * @return RedirectResponse
     */
    public function remove(Organization $organization): RedirectResponse
    {
        $this->organizationService->delete($organization);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.settings.organizations');
    }
}
