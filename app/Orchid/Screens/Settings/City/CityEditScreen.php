<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Settings\City;

use App\Http\Requests\Settings\City\StoreCityRequest;
use App\Models\City;
use App\Orchid\Layouts\Settings\City\CityEditLayout;
use App\Services\CityService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CityEditScreen extends Screen
{
    public City $city;

    public function __construct(private CityService $cityService){

    }

    /**
     * Query data.
     *
     * @param City $city
     *
     * @return array
     */
    public function query(City $city): iterable
    {
        return [
            'city'  => $city,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.settings.cities',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->city->exists ? 'Редактирование города ' : 'Создание города';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->city->exists ? 'Редактирование города ' : 'Создание города';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->city->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                CityEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. $this->city->exists ? 'отредактировать' : 'создать новый' . ' город')

        ];
    }

    /**
     * @param City $city
     * @param StoreCityRequest $request
     *
     * @return RedirectResponse
     */
    public function save(City $city, StoreCityRequest $request): RedirectResponse
    {
        $city->exists
            ? $this->cityService->update($city, $request->validated())
            : $this->cityService->create($request->validated());

        Toast::info('Запись успешно ' . ($city->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.settings.cities');
    }

    /**
     * @param City $city
     *
     * @return RedirectResponse
     */
    public function remove(City $city): RedirectResponse
    {
        $this->cityService->delete($city);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.settings.cities');
    }
}
