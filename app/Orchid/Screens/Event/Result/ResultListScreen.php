<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Event\Result;

use App\Models\Activity;
use App\Models\Event\Result;
use App\Orchid\Layouts\Event\Result\ResultListLayout;
use App\Services\ActivityClassificationService;
use Illuminate\Http\Request;
use Orchid\Platform\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Modal;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class ResultListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'results' => Result::query()
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(10),
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Результаты';
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.event.results',
        ];
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Все установленные резултаты в системе';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.event.result.create'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            ResultListLayout::class,
            Layout::modal('exampleModal', [
                Layout::view('modals.activity-classification-modal'),
            ])->size(Modal::SIZE_LG)
                ->type(Modal::TYPE_RIGHT)
                ->withoutApplyButton()
                ->title('результаты')
                ->async('asyncGetActivitiesClassificationToTable'),
        ];
    }

    /**
     * @param Activity $activity
     * @param string $result
     * @return array|array[]
     */
    public function asyncGetActivitiesClassificationToTable(Activity $activity, string $result): array
    {
        return (new ActivityClassificationService())->getActivitiesClassificationToTable($activity, $result);
    }

    /**
     * @param Request $request
     * @param User    $user
     */
    public function save(Request $request, User $user): void
    {

    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {

    }
}
