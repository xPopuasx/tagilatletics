<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Event\Result;

use App\Http\Requests\Event\StoreResultRequest;
use App\Models\Event\Result;
use App\Orchid\Layouts\Event\Result\ResultEditLayout;
use App\Services\Event\ResultService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ResultEditScreen extends Screen
{
    public Result $result;

    public function __construct(private ResultService $resultService){

    }

    /**
     * Query data.
     *
     * @param Result $result
     *
     * @return array
     */
    public function query(Result $result): iterable
    {
        return [
            'result'  => $result,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.event.results',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->result->exists ? 'Редактирование результата ' : 'Создание результата';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->result->exists ? 'Редактирование результата ' : 'Создание результата';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->result->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                ResultEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->result->exists ? 'отредактировать' : 'создать новуый') . ' результат <br>
                <b>Обратите внимание что необходимо полностью заполнять временные поля или те которые отвечают а результат в метрах</b>')

        ];
    }

    /**
     * @param Result $result
     * @param StoreResultRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Result $result, StoreResultRequest $request): RedirectResponse
    {
        $result->exists
            ? $this->resultService->update($result, $request->validated())
            : $this->resultService->create($request->validated());

        Toast::info('Запись успешно ' . ($result->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.event.results');
    }

    /**
     * @param Result $result
     *
     * @return RedirectResponse
     */
    public function remove(Result $result): RedirectResponse
    {
        $this->resultService->delete($result);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.event.results');
    }
}
