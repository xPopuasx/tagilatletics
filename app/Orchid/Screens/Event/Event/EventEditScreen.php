<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Event\Event;

use App\Http\Requests\Member\StoreEventRequest;
use App\Models\Event\Event;
use App\Orchid\Layouts\Event\Event\EventEditLayout;
use App\Services\Event\EventService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class EventEditScreen extends Screen
{
    public Event $event;

    public function __construct(private EventService $eventService){

    }

    /**
     * Query data.
     *
     * @param Event $event
     *
     * @return array
     */
    public function query(Event $event): iterable
    {
        return [
            'event'  => $event,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.event.events',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->event->exists ? 'Редактирование дисциплины ' : 'Создание дисциплины';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->event->exists ? 'Редактирование дисциплины ' : 'Создание дисциплины';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->event->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                EventEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. $this->event->exists ? 'отредактировать' : 'создать нову' . ' событие')

        ];
    }

    /**
     * @param Event $event
     * @param StoreEventRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Event $event, StoreEventRequest $request): RedirectResponse
    {
        $event->exists
            ? $this->eventService->update($event, $request->validated())
            : $this->eventService->create($request->validated());

        Toast::info('Запись успешно ' . ($event->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.event.events');
    }

    /**
     * @param Event $event
     *
     * @return RedirectResponse
     */
    public function remove(Event $event): RedirectResponse
    {
        $this->eventService->delete($event);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.event.events');
    }
}
