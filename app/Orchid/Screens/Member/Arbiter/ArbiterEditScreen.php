<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Arbiter;

use App\Http\Requests\Member\StoreArbiterRequest;
use App\Models\Member\Arbiter;
use App\Orchid\Layouts\Member\Arbiter\ArbiterEditLayout;
use App\Orchid\Layouts\Member\Arbiter\Category\ArbiterCategoryEditLayout;
use App\Services\Member\ArbiterService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ArbiterEditScreen extends Screen
{
    public Arbiter $arbiter;

    public function __construct(private ArbiterService $arbiterService){

    }

    /**
     * Query data.
     *
     * @param Arbiter $arbiter
     *
     * @return array
     */
    public function query(Arbiter $arbiter): iterable
    {
        return [
            'arbiter'  => $arbiter,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.arbiters',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->arbiter->exists ? 'Редактирование судьи' : 'Создание судьи';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->arbiter->exists ? 'Редактирование судьи' : 'Создание судьи';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->arbiter->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                ArbiterEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->arbiter->exists ? 'отредактировать' : 'создать нового') . ' судью')

        ];
    }

    /**
     * @param Arbiter $arbiter
     * @param StoreArbiterRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Arbiter $arbiter, StoreArbiterRequest $request): RedirectResponse
    {
        $arbiter->exists
            ? $this->arbiterService->update($arbiter, $request->validated())
            : $this->arbiterService->create($request->validated());

        Toast::info('Запись успешно ' . ($arbiter->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.arbiters');
    }

    /**
     * @param Arbiter $arbiter
     *
     * @return RedirectResponse
     */
    public function remove(Arbiter $arbiter): RedirectResponse
    {
        $this->arbiterService->delete($arbiter);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.arbiters');
    }
}
