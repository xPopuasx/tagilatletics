<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Arbiter\Category;

use App\Models\Member\ArbiterCategory;
use App\Orchid\Layouts\Member\Arbiter\Category\ArbiterCategoryListLayout;
use Illuminate\Http\Request;
use Orchid\Platform\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ArbiterCategoryListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'arbiterCategories' => ArbiterCategory::query()
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(10),
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.arbiter.categories',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Судейские категории';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Все установленные судейские категории в системе';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.member.arbiter.category.create'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            ArbiterCategoryListLayout::class
        ];
    }

    /**
     * @param Request $request
     * @param User    $user
     */
    public function save(Request $request, User $user): void
    {

    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {

    }
}
