<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Arbiter\Category;

use App\Http\Requests\Member\StoreArbiterCategoryRequest;
use App\Models\Member\ArbiterCategory;
use App\Orchid\Layouts\Member\Arbiter\Category\ArbiterCategoryEditLayout;
use App\Services\Member\ArbiterCategoryService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ArbiterCategoryEditScreen extends Screen
{
    public ArbiterCategory $arbiterCategory;

    public function __construct(private ArbiterCategoryService $arbiterCategoryService){

    }

    /**
     * Query data.
     *
     * @param ArbiterCategory $arbiterCategory
     *
     * @return array
     */
    public function query(ArbiterCategory $arbiterCategory): iterable
    {
        return [
            'arbiterCategory'  => $arbiterCategory,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.arbiter.categories',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->arbiterCategory->exists ? 'Редактирование судейской категории ' : 'Создание судейской категории';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->arbiterCategory->exists ? 'Редактирование судейской категории ' : 'Создание судейской категории';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->arbiterCategory->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                ArbiterCategoryEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->arbiterCategory->exists ? 'отредактировать' : 'создать нову') . ' судейскую категорию')

        ];
    }

    /**
     * @param ArbiterCategory $arbiterCategory
     * @param StoreArbiterCategoryRequest $request
     *
     * @return RedirectResponse
     */
    public function save(ArbiterCategory $arbiterCategory, StoreArbiterCategoryRequest $request): RedirectResponse
    {
        $arbiterCategory->exists
            ? $this->arbiterCategoryService->update($arbiterCategory, $request->validated())
            : $this->arbiterCategoryService->create($request->validated());

        Toast::info('Запись успешно ' . ($arbiterCategory->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.arbiter.categories');
    }

    /**
     * @param ArbiterCategory $arbiterCategory
     *
     * @return RedirectResponse
     */
    public function remove(ArbiterCategory $arbiterCategory): RedirectResponse
    {
        $this->arbiterCategoryService->delete($arbiterCategory);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.arbiter.categories');
    }
}
