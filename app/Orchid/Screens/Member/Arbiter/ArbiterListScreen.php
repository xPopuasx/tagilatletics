<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Arbiter;

use App\Models\Member\Arbiter;
use App\Models\Member\ArbiterCategory;
use App\Orchid\Layouts\Member\Arbiter\ArbiterListLayout;
use App\Orchid\Layouts\Member\Arbiter\Category\ArbiterCategoryListLayout;
use Illuminate\Http\Request;
use Orchid\Platform\Models\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ArbiterListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'arbiters' => Arbiter::query()
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(10),
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.arbiters',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Судьи';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Все установленные судьи в системе';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.member.arbiter.create'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            ArbiterListLayout::class
        ];
    }

    /**
     * @param Request $request
     * @param User    $user
     */
    public function save(Request $request, User $user): void
    {

    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {

    }
}
