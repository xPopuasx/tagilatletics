<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Athlete;

use App\Http\Requests\Member\StoreAthleteRequest;
use App\Models\Activity;
use App\Models\Event\Result;
use App\Models\Member\Athlete;
use App\Orchid\Layouts\Event\Result\ResultListLayout;
use App\Orchid\Layouts\Member\Athlete\AthleteEditLayout;
use App\Services\ActivityClassificationService;
use App\Services\Member\AthleteService;
use Illuminate\Http\Client\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Route;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layouts\Modal;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class AthleteEditScreen extends Screen
{
    public Athlete $athlete;

    public function __construct(private AthleteService $athleteService){

    }

    /**
     * Query data.
     *
     * @param Athlete $athlete
     *
     * @return array
     */
    public function query(Athlete $athlete): iterable
    {
        return [
            'athlete'  => $athlete,
            'results' => Result::query()
                ->where('athlete_id', $athlete->id)
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(10),
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.athletes',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->athlete->exists ? 'Редактирование атлета' : 'Создание атлета';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->athlete->exists ? 'Редактирование атлета' : 'Создание атлета';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->athlete->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                AthleteEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. (!Request()->ajax() && $this->athlete->exists ? 'отредактировать' : 'создать нового') . ' атлета'),

            ResultListLayout::class,

            Layout::modal('exampleModal', [
                Layout::view('modals.activity-classification-modal'),
            ])->size(Modal::SIZE_LG)
                ->type(Modal::TYPE_RIGHT)
                ->withoutApplyButton()
                ->title('результаты')
                ->async('asyncGetActivitiesClassificationToTable'),
        ];
    }

    /**
     * @param Activity $activity
     * @param string $result
     * @return array|array[]
     */
    public function asyncGetActivitiesClassificationToTable(Activity $activity, string $result): array
    {
        return (new ActivityClassificationService())->getActivitiesClassificationToTable($activity, $result);
    }

    /**
     * @param Athlete $athlete
     * @param StoreAthleteRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Athlete $athlete, StoreAthleteRequest $request): RedirectResponse
    {
        $athlete->exists
            ? $this->athleteService->update($athlete, $request->validated())
            : $this->athleteService->create($request->validated());

        Toast::info('Запись успешно ' . ($athlete->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.athletes');
    }

    /**
     * @param Athlete $athlete
     *
     * @return RedirectResponse
     */
    public function remove(Athlete $athlete): RedirectResponse
    {
        $this->athleteService->delete($athlete);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.athletes');
    }
}
