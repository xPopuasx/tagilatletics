<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Athlete\RegionTeam;

use App\Http\Requests\Member\StoreRegionTeamRequest;
use App\Models\Member\RegionTeam;
use App\Orchid\Layouts\Member\Athlete\RegionTeam\RegionTeamEditLayout;
use App\Services\Member\RegionTeamService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class RegionTeamEditScreen extends Screen
{
    public RegionTeam $regionTeam;

    public function __construct(private RegionTeamService $regionTeamService){

    }

    /**
     * Query data.
     *
     * @param RegionTeam $regionTeam
     *
     * @return array
     */
    public function query(RegionTeam $regionTeam): iterable
    {
        return [
            'regionTeam'  => $regionTeam,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.athlete.regionTeams',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->regionTeam->exists ? 'Редактирование сборной области' : 'Создание сборной области';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->regionTeam->exists ? 'Редактирование сборной области' : 'Создание сборной области';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->regionTeam->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                RegionTeamEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->regionTeam->exists ? 'отредактировать' : 'создать новую') . ' сборную области')

        ];
    }

    /**
     * @param RegionTeam $regionTeam
     * @param StoreRegionTeamRequest $request
     *
     * @return RedirectResponse
     */
    public function save(RegionTeam $regionTeam, StoreRegionTeamRequest $request): RedirectResponse
    {
        $regionTeam->exists
            ? $this->regionTeamService->update($regionTeam, $request->validated())
            : $this->regionTeamService->create($request->validated());

        Toast::info('Запись успешно ' . ($regionTeam->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.athlete.regionTeams');
    }

    /**
     * @param RegionTeam $regionTeam
     *
     * @return RedirectResponse
     */
    public function remove(RegionTeam $regionTeam): RedirectResponse
    {
        $this->regionTeamService->delete($regionTeam);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.athlete.regionTeams');
    }
}
