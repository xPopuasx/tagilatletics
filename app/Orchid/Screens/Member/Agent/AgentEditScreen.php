<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Agent;

use App\Http\Requests\Member\StoreAgentRequest;
use App\Models\member\Agent;
use App\Orchid\Layouts\Member\Agent\AgentEditLayout;
use App\Services\Member\AgentService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class AgentEditScreen extends Screen
{
    public Agent $agent;

    public function __construct(private AgentService $agentService){

    }

    /**
     * Query data.
     *
     * @param Agent $agent
     *
     * @return array
     */
    public function query(Agent $agent): iterable
    {
        return [
            'agent'  => $agent,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.agents',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->agent->exists ? 'Редактирование агента ' : 'Создание агента';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->agent->exists ? 'Редактирование агента ' : 'Создание агента';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->agent->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                AgentEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->agent->exists ? 'отредактировать' : 'создать нового') . ' агетра')

        ];
    }

    /**
     * @param Agent $agent
     * @param StoreAgentRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Agent $agent, StoreAgentRequest $request): RedirectResponse
    {
        $agent->exists
            ? $this->agentService->update($agent, $request->validated())
            : $this->agentService->create($request->validated());

        Toast::info('Запись успешно ' . ($agent->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.agents');
    }

    /**
     * @param Agent $agent
     *
     * @return RedirectResponse
     */
    public function remove(Agent $agent): RedirectResponse
    {
        $this->agentService->delete($agent);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.agents');
    }
}
