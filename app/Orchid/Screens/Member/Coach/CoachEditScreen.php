<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Coach;

use App\Http\Requests\Member\StoreCoachRequest;
use App\Models\Member\Coach;
use App\Orchid\Layouts\Member\Coach\CoachEditLayout;
use App\Services\Member\CoachService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CoachEditScreen extends Screen
{
    public Coach $coach;

    public function __construct(private CoachService $coachService){

    }

    /**
     * Query data.
     *
     * @param Coach $coach
     *
     * @return array
     */
    public function query(Coach $coach): iterable
    {
        return [
            'coach'  => $coach,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.coaches',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->coach->exists ? 'Редактирование тренера' : 'Создание тренера';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->coach->exists ? 'Редактирование тренера' : 'Создание тренера';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->coach->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                CoachEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->coach->exists ? 'отредактировать' : 'создать нового') . ' тренера')

        ];
    }

    /**
     * @param Coach $coach
     * @param StoreCoachRequest $request
     *
     * @return RedirectResponse
     */
    public function save(Coach $coach, StoreCoachRequest $request): RedirectResponse
    {
        $coach->exists
            ? $this->coachService->update($coach, $request->validated())
            : $this->coachService->create($request->validated());

        Toast::info('Запись успешно ' . ($coach->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.coaches');
    }

    /**
     * @param Coach $coach
     *
     * @return RedirectResponse
     */
    public function remove(Coach $coach): RedirectResponse
    {
        $this->coachService->delete($coach);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.coaches');
    }
}
