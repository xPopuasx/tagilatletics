<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Member\Coach\Category;

use App\Http\Requests\Member\StoreCoachCategoryRequest;
use App\Models\Member\CoachCategory;
use App\Orchid\Layouts\Member\Arbiter\Category\ArbiterCategoryEditLayout;
use App\Orchid\Layouts\Member\Coach\Category\CoachCategoryEditLayout;
use App\Services\Member\CoachCategoryService;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CoachCategoryEditScreen extends Screen
{
    public CoachCategory $coachCategory;

    public function __construct(private CoachCategoryService $coachCategoryService){

    }

    /**
     * Query data.
     *
     * @param CoachCategory $coachCategory
     *
     * @return array
     */
    public function query(CoachCategory $coachCategory): iterable
    {
        return [
            'coachCategory'  => $coachCategory,
        ];
    }

    /**
     * @return iterable|null
     */
    public function permission(): ?iterable
    {
        return [
            'platform.member.coach.categories',
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->coachCategory->exists ? 'Редактирование тренерской категории ' : 'Создание тренерской категории';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->coachCategory->exists ? 'Редактирование тренерской категории ' : 'Создание тренерской категории';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->method('save'),

            Button::make(__('Удалить'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->coachCategory->exists),
        ];
    }

    /**
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [

            Layout::block([
                CoachCategoryEditLayout::class,
            ])
                ->title('Информация')
                ->description('Здесь вы можете '. ($this->coachCategory->exists ? 'отредактировать' : 'создать нову') . ' тренерскую категорию')

        ];
    }

    /**
     * @param CoachCategory $coachCategory
     * @param StoreCoachCategoryRequest $request
     *
     * @return RedirectResponse
     */
    public function save(CoachCategory $coachCategory, StoreCoachCategoryRequest $request): RedirectResponse
    {
        $coachCategory->exists
            ? $this->coachCategoryService->update($coachCategory, $request->validated())
            : $this->coachCategoryService->create($request->validated());

        Toast::info('Запись успешно ' . ($coachCategory->exists ? 'отредактирована' : 'добавлена'));

        return redirect()->route('platform.member.coach.categories');
    }

    /**
     * @param CoachCategory $coachCategory
     *
     * @return RedirectResponse
     */
    public function remove(CoachCategory $coachCategory): RedirectResponse
    {
        $this->coachCategoryService->delete($coachCategory);

        Toast::info('Запись успешно удалена');

        return redirect()->route('platform.member.coach.categories');
    }
}
