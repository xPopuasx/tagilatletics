<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\Organization;

use App\Services\CityService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class OrganizationEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('organization.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название организации'))
                ->placeholder('Укажите название организации')
                ->help(__('По данному полю вы будите ориентироваться в поиске организации')),

            Input::make('organization.phone')
                ->type('text')
                ->max(255)
                ->mask('+7 (999) 999-99-99')
                ->title(__('Телефон организации'))
                ->placeholder('Укажите телефон организации')
                ->help(__('По данному полю вы будите ориентироваться в поиске организации')),

            Input::make('organization.address')
                ->type('text')
                ->max(255)
                ->title(__('Адрес организации'))
                ->placeholder('Укажите адрес организации')
                ->help(__('По данному полю вы будите ориентироваться в поиске организации')),

            Input::make('organization.site_address')
                ->type('text')
                ->max(255)
                ->title(__('Ссылка на сайт организации'))
                ->placeholder('Укажите ссылку на сайт организации')
                ->help(__('По данному полю вы будите ориентироваться в поиске организации')),

            Input::make('organization.director_fil')
                ->type('text')
                ->max(255)
                ->title(__('Директор организации'))
                ->placeholder('Укажите директора организации')
                ->help(__('По данному полю вы будите ориентироваться в поиске организации')),

            Select::make('organization.city_id')
                ->options(array_merge(
                    ['no_value' => 'Выбрать город'],
                    (new CityService())->getCitiesToSelect()
                ))
                ->title(__('Выбрать город'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске организации')),

            Cropper::make('organization.image_url')
                ->groups(config('constants.image_types.organization'))
                ->title('Логотип организации')
        ];
    }
}
