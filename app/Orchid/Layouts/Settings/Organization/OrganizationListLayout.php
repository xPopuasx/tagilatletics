<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\Organization;

use App\Models\Activity;
use App\Models\City;
use App\Models\Organization;
use App\Orchid\Layouts\Settings\Organization\Column\SingleColumn;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class OrganizationListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'organizations';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return $organization->id;
                }),

            TD::make('name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return new SingleColumn($organization);
                }),

            TD::make('phone', __('phone'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return $organization->phone ?? '-';
                }),

            TD::make('email', __('Почта'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return $organization->email ?? '-';
                }),

            TD::make('city.name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return $organization->city->name ?? '-';
                }),

            TD::make('city.region_name', 'Регион')
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return $organization->city->region_name ?? '-';
                }),

            TD::make('city.county', 'Округ')
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Organization $organization) {
                    return $organization->city->county ?? '-';
                }),
        ];
    }
}
