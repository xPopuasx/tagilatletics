<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\Organization\Column;

use App\Models\ActivityClassification;
use App\Models\Member\Arbiter;
use App\Models\Organization;
use Illuminate\View\View;
use Orchid\Screen\Layouts\Content;

class SingleColumn extends Content
{
    /**
     * @var string
     */
    protected $template = 'platform::layouts.persona';

    /**
     * @param Organization $organization
     *
     * @return View
     */
    public function render(Organization $organization): View
    {
        return view($this->template, [
            'title'    => $organization->name,
            'subTitle' => 'Сайт : '. ($organization->site_address ?? 'не указан'),
            'image'    => $organization->image_url ?? asset('/vendor/default_images/default_organization.jpg'),
            'url'      => route('platform.settings.organization.edit', ['organization' => $organization->id ?? 0]),
        ]);
    }
}
