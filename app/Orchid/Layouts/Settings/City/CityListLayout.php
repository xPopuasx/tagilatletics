<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\City;

use App\Models\Activity;
use App\Models\City;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CityListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'cities';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (City $city) {
                    return $city->id;
                }),

            TD::make('name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (City $city) {
                    return $city->name;
                }),

            TD::make('region_name', 'Регион')
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (City $city) {
                    return $city->region_name;
                }),

            TD::make('county', 'Округ')
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (City $city) {
                    return $city->county;
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (City $city) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.settings.cities.edit', ['city' => $city->id ?? 0])
                        ]);
                }),
        ];
    }
}
