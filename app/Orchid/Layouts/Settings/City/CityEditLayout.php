<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\City;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class CityEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('city.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название города'))
                ->placeholder('Укажите название города')
                ->help(__('По данному полю вы будите ориентироваться в поиске города')),

            Input::make('city.region_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название региона'))
                ->placeholder('Укажите название региона')
                ->help(__('По данному полю вы будите ориентироваться в поиске города')),

            Input::make('city.county')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название округа'))
                ->placeholder('Укажите название округа')
                ->help(__('По данному полю вы будите ориентироваться в поиске города')),
        ];
    }
}
