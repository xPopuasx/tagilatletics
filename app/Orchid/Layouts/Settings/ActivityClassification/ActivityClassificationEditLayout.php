<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\ActivityClassification;

use App\Services\ActivityClassificationCriterionService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class ActivityClassificationEditLayout extends Rows
{

    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('activityClassification.name')
                ->type('text')
                ->required()
                ->title(__('Название классификации'))
                ->placeholder('Укажите название классификации')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.unit')
                ->title(__('Название единицы измерения'))
                ->placeholder('Укажите название дисциплины')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Select::make('activityClassification.criterion_id')
                ->options(array_merge(
                    ['no_value' => 'Выбрать критерий'],
                    (new ActivityClassificationCriterionService())->getCriteriaToSelect()
                ))
                ->title(__('Выбрать критерий'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_msmk')
                ->title(__('Результат мастера спорта международного класса'))
                ->placeholder('Укажите результат мастера спорта международного класса')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_ms')
                ->type('text')
                ->title(__('Результат мастера спорта'))
                ->placeholder('Укажите результат мастера спорта')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_kms')
                ->type('text')
                ->title(__('Результат кандитата в мастера спорта'))
                ->placeholder('Укажите результат кандитата в мастера спорта')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_first_adult')
                ->type('text')
                ->title(__('Результат I взрослого разраяда'))
                ->placeholder('Укажите результат I взрослого разраяда')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_second_adult')
                ->type('text')
                ->title(__('Результат II взрослого разраяда'))
                ->placeholder('Укажите результат II взрослого разраяда')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_third_adult')
                ->type('text')
                ->title(__('Результат III взрослого разраяда'))
                ->placeholder('Укажите результат III взрослого разраяда')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_first_youthful')
                ->type('text')
                ->title(__('Результат I юношеского разраяда'))
                ->placeholder('Укажите результат I юношеского разраяда')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_second_youthful')
                ->type('text')
                ->title(__('Результат II юношеского разраяда'))
                ->placeholder('Укажите результат II юношеского разраяда')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),

            Input::make('activityClassification.discharge_third_youthful')
                ->type('text')
                ->title(__('Результат III юношеского разраяда'))
                ->placeholder('Укажите результат I юношеского разраяда')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),
        ];
    }
}
