<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\ActivityClassification;

use App\Models\ActivityClassification;
use App\Orchid\Layouts\Settings\ActivityClassification\Column\SingleColumn;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ActivityClassificationListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'activityClassifications';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->width(50)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->id;
                }),

            TD::make('name', __('Name'))
                ->sort()
                ->width(150)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return new SingleColumn($activityClassification);
                }),

            TD::make('unit', __('ед. изм.'))
                ->sort()
                ->cantHide()
                ->width(80)
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->unit;
                }),

            TD::make('discharge_msmk', __('МСМК'))
                ->sort()
                ->cantHide()
                ->width(80)
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_msmk;
                }),

            TD::make('discharge_ms', __('МС'))
                ->sort()
                ->cantHide()
                ->width(80)
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_ms;
                }),

            TD::make('discharge_kms', __('КМС'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_kms;
                }),

            TD::make('discharge_first_adult', __('I В.'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_first_adult;
                }),

            TD::make('discharge_second_adult', __('II В.'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_second_adult;
                }),

            TD::make('discharge_third_adult', __('III В.'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_third_adult;
                }),

            TD::make('discharge_first_youthful', __('I Ю.'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_first_youthful;
                }),

            TD::make('discharge_second_youthful', __('II Ю.'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_second_youthful;
                }),

            TD::make('discharge_third_youthful', __('III Ю.'))
                ->sort()
                ->width(80)
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassification $activityClassification) {
                    return $activityClassification->discharge_third_youthful;
                }),
        ];
    }
}
