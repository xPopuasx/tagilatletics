<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\ActivityClassification\Criterion;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class ActivityClassificationCriterionEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('activityClassificationCriterion.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название классификации'))
                ->placeholder('Укажите название классификации')
                ->help(__('По данному полю вы будите ориентироваться в поиске классификации')),
        ];
    }
}
