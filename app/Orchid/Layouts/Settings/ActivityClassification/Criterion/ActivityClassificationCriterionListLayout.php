<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\ActivityClassification\Criterion;

use App\Models\Activity;
use App\Models\ActivityClassification;
use App\Models\ActivityClassificationCriterion;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ActivityClassificationCriterionListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'activityClassificationCriterion';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassificationCriterion $activityClassificationCriterion) {
                    return $activityClassificationCriterion->id;
                }),

            TD::make('name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ActivityClassificationCriterion $activityClassificationCriterion) {
                    return $activityClassificationCriterion->name;
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (ActivityClassificationCriterion $activityClassificationCriterion) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.settings.activity.classification.criteria.edit', ['activityClassificationCriterion' => $activityClassificationCriterion->id ?? 0])
                        ]);
                }),
        ];
    }
}
