<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\ActivityClassification\Column;

use App\Models\ActivityClassification;
use Illuminate\View\View;
use Orchid\Screen\Layouts\Content;

class SingleColumn extends Content
{
    /**
     * @var string
     */
    protected $template = 'platform::layouts.persona';

    /**
     * @param ActivityClassification $activityClassification
     *
     * @return View
     */
    public function render(ActivityClassification $activityClassification): View
    {
        return view($this->template, [
            'title'    => $activityClassification->name,
            'subTitle' => 'Критерий: '. (($activityClassification->criterion) ? $activityClassification->criterion->name : 'не указан'),
            'image'    => '',
            'url'      => route('platform.settings.activity.classification.edit', ['activityClassification' => $activityClassification->id ?? 0]),
        ]);
    }
}
