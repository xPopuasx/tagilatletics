<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\Activity;

use App\Models\Activity;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ActivityListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'activities';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Activity $activity) {
                    return $activity->id;
                }),

            TD::make('name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Activity $activity) {
                    return $activity->name;
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Activity $activity) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.settings.activities.edit', ['activity' => $activity->id ?? 0])
                        ]);
                }),
        ];
    }
}
