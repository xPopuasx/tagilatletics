<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Settings\Activity;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class ActivityEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('activity.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название дисциплины'))
                ->placeholder('Укажите название дисциплины')
                ->help(__('По данному полю вы будите ориентироваться в поиске дисциплины')),
        ];
    }
}
