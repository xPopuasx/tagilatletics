<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Event\Event;

use App\Models\member\Agent;
use App\Services\CityService;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class EventEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            TextArea::make('event.name')
                ->max(255)
                ->required()
                ->title(__('Название мероприятия'))
                ->placeholder('Укажите название мероприятия')
                ->help(__('По данному полю вы будите ориентироваться в поиске мероприятий')),

            Select::make('event.city_id')
                ->options((new CityService())->getCitiesToSelect())
                ->title(__('Выбрать город'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске мероприятий')),

            Input::make('event.place_name')
                ->type('text')
                ->max(255)
                ->title(__('Название места проведения'))
                ->placeholder('Укажите название места проведения')
                ->help(__('По данному полю вы будите ориентироваться в поиске мероприятий')),

            DateTimer::make('event.date_start')
                ->format('Y-m-d')
                ->title(__('Дата до'))
                ->placeholder('Укажите дату до')
                ->help(__('По данному полю вы будите ориентироваться в поиске мероприятий')),

            DateTimer::make('event.date_end')
                ->format('Y-m-d')
                ->title(__('Дата до'))
                ->placeholder('Укажите дату до')
                ->help(__('По данному полю вы будите ориентироваться в поиске мероприятий')),
        ];
    }
}
