<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Event\Event;

use App\Models\Activity;
use App\Models\Event\Event;
use App\Models\member\Agent;
use App\Models\Organization;
use Carbon\Carbon;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class EventListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'events';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Event $event) {
                    return $event->id;
                }),

            TD::make('name', __('Название события'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->width(350)
                ->render(function (Event $event) {
                    return $event->name ?? '-';
                }),

            TD::make('city_id', __('Город'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Event $event) {
                    return $event->city->name ?? '-';
                }),

            TD::make('place_name', __('Название метса'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Event $event) {
                    return $event->place_name ?? '-';
                }),

            TD::make('date_start', __('Дата начала'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Event $event) {
                    return Carbon::parse($event->date_start)->format('d.m.Y');
                }),

            TD::make('date_end', __('Дата окончания'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Event $event) {
                    return Carbon::parse($event->date_end)->format('d.m.Y');
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Event $event) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.event.event.edit', ['event' => $event->id ?? 0])
                        ]);
                }),
        ];
    }
}
