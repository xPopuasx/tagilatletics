<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Event\Result;

use App\Models\member\Agent;
use App\Models\Member\Athlete;
use App\Services\ActivityService;
use App\Services\CityService;
use App\Services\Event\EventService;
use App\Services\Member\AthleteService;
use App\Services\Member\CoachService;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class ResultEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Select::make('result.athlete_id')
                ->required()
                ->options((new AthleteService())->getAthleteToSelect())
                ->title(__('Выбрать атлета'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске результата')),

            Select::make('result.coach_id')
                ->options((new CoachService())->getCoachesToSelect())
                ->title(__('Выбрать тренера'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске результата')),

            Select::make('result.activity_id')
                ->required()
                ->options((new ActivityService())->getActivitiesToSelect())
                ->title(__('Выбрать дисциплину'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске результата')),

            Select::make('result.event_id')
                ->required()
                ->options((new EventService())->getEventsToSelect())
                ->title(__('Выбрать событие'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске результата')),

            Input::make('result.points')
                ->type('number')
                ->title('Очки'),

            Input::make('result.athlete_place')
                ->type('number')
                ->title('Место'),

            Input::make('result.result_hours')
                ->title('Результат часы')
                ->mask('99'),

            Input::make('result.result_minutes')
                ->title('Результат минуты')
                ->mask('99'),

            Input::make('result.result_seconds')
                ->title('Результат секунды')
                ->mask('99'),

            Input::make('result.result_milliseconds')
                ->title('Результат милисекунды')
                ->mask('99'),

            Input::make('result.result_in_meters')
                ->title('Результат метров'),
        ];
    }
}
