<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Event\Result;

use App\Models\Activity;
use App\Models\ActivityClassification;
use App\Models\Event\Event;
use App\Models\Event\Result;
use App\Models\member\Agent;
use App\Models\Organization;
use Carbon\Carbon;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ResultListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'results';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Result $result) {
                    return $result->id;
                }),

            TD::make('athlete_id', __('Атлет'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Result $result) {
                    return $result->athlete->fio ?? '-';
                }),

            TD::make('coach_id', __('Основной тренер'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Result $result) {
                    return $result->coach->fio ?? '-';
                }),

            TD::make('activity_id', __('Дисциплина'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Result $result) {
                    return $result->activity->name ?? '-';
                }),

            TD::make('activity_id', __('Результат (ч:м:c.мс)'))
                ->render(function (Result $result) {
                    return $result->time;
                }),

            TD::make('result_in_meters', __('Результат метры'))
                ->render(function (Result $result) {
                    return $result->result_in_meters ?? '-';
                }),

            TD::make('activity_id', __('Разряды'))
                ->render(function (Result $result) {
                    if(count($result->activity->findClassification) != 0){
                        return ModalToggle::make()
                            ->modal('exampleModal')
                            ->icon('info')
                            ->asyncParameters([$result->activity->id, $result->result_in_meters ?? $result->time]);
                    } else {
                        return 'не найдено';
                    }
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Result $result) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.event.result.edit', ['result' => $result->id ?? 0])
                        ]);
                }),
        ];
    }
}
