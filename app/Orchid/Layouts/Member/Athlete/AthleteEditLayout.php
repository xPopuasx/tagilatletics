<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Athlete;

use App\Models\member\Agent;
use App\Models\Member\Arbiter;
use App\Models\Member\Athlete;
use App\Services\ActivityClassificationCriterionService;
use App\Services\ActivityService;
use App\Services\Member\ArbiterCategoryService;
use App\Services\Member\CoachService;
use App\Services\Member\RegionTeamService;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class AthleteEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('athlete.last_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Имя атлета'))
                ->placeholder('Укажите имя атлета')
                ->help(__('По данному полю вы будите ориентироваться в поиске атлета')),

            Input::make('athlete.first_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Фамилия атлета'))
                ->placeholder('Укажите фамилия атлета')
                ->help(__('По данному полю вы будите ориентироваться в поиске атлета')),

            Input::make('athlete.patronymic')
                ->type('text')
                ->title(__('Отчетсво атлета'))
                ->placeholder('Укажите отчество атлета')
                ->help(__('По данному полю вы будите ориентироваться в поиске атлета')),

            DateTimer::make('athlete.birth_date')
                ->format('Y-m-d')
                ->title(__('Дата рождения атлета'))
                ->placeholder('Укажите дату рождения атлета')
                ->help(__('По данному полю вы будите ориентироваться в поиске атлета')),

            Select::make('athlete.gender')
                ->options([
                    'male'   => 'Мужской',
                    'female' => 'Женский',
                ])
                ->title('Выбрать пол атлета'),

            Input::make('athlete.qualification')
                ->type('text')
                ->max(255)
                ->title(__('Квалификация атлета'))
                ->placeholder('Укажите Квалификацию атлета')
                ->help(__('По данному полю вы будите ориентироваться в поиске атлета')),

            Select::make('athlete.region_team_id')
                ->options((new RegionTeamService())->getRegionTeamsToSelect())
                ->title(__('Выбрать сборную области'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске атлета')),

            Select::make('athlete.coaches.')
                ->fromModel(Athlete::class, 'id')
                ->options((new CoachService())->getCoachesToSelect())
                ->multiple()
                ->title(__('Выбрать тренера (-ов)'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске атлета')),

            Select::make('athlete.organizations.')
                ->fromModel(Athlete::class, 'id')
                ->options((new OrganizationService())->getOrganizationsToSelect())
                ->multiple()
                ->title(__('Выбрать организацию (-и)'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске атлета')),

            Select::make('athlete.activities.')
                ->fromModel(Athlete::class, 'id')
                ->options((new ActivityService())->getActivitiesToSelect())
                ->multiple()
                ->title(__('Выбрать специальзацию'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске атлета')),

            Switcher::make('athlete.umo')
                ->sendTrueOrFalse()
                ->title('Углубленное медицинское обследование')
                ->placeholder('Углубленное медицинское обследование'),

            Cropper::make('athlete.image_url')
                ->groups(config('constants.image_types.athlete'))
                ->title('Изображение атлета'),

            Upload::make('athlete.attachment')
                ->title('Страховка')
                ->groups(config('constants.image_types.athlete_insurance'))
                ->maxFiles(1)
                ->help('Добавить страховку'),

            Upload::make('athlete.attachment')
                ->title('РУСАДА')
                ->groups(config('constants.image_types.athlete_rusada'))
                ->maxFiles(1)
                ->help('Добавить заключение российской национальной антидопинговой комиссии')
        ];
    }
}
