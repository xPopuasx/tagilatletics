<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Athlete\RegionTeam;

use App\Models\member\Agent;
use App\Models\Member\Arbiter;
use App\Models\Member\Athlete;
use App\Services\ActivityClassificationCriterionService;
use App\Services\ActivityService;
use App\Services\Member\ArbiterCategoryService;
use App\Services\Member\CoachService;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class RegionTeamEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('regionTeam.region_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название регоина'))
                ->placeholder('Укажите название регоина')
                ->help(__('По данному полю вы будите ориентироваться в поиске сборной области')),

            Input::make('regionTeam.age_up_to')
                ->type('number')
                ->max(255)
                ->required()
                ->title(__('Возрастная категория'))
                ->placeholder('Укажите возрастную категорию')
                ->help(__('По данному полю вы будите ориентироваться в поиске сборной области')),
        ];
    }
}
