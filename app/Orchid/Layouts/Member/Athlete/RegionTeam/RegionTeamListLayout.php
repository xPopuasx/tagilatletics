<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Athlete\RegionTeam;

use App\Models\Activity;
use App\Models\Member\Arbiter;
use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use App\Models\Member\RegionTeam;
use App\Models\Organization;
use App\Orchid\Layouts\Member\Athlete\Column\SingleColumn;
use Carbon\Carbon;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class RegionTeamListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'regionTeams';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (RegionTeam $regionTeam) {
                    return $regionTeam->id;
                }),

            TD::make('region_name', __('Название региона'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (RegionTeam $regionTeam) {
                    return $regionTeam->region_name;
                }),

            TD::make('age_up_to', __('Возрастная категория'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (RegionTeam $regionTeam) {
                    return $regionTeam->age_up_to;
                }),

            TD::make('athletes_count', __('Количество участников'))
                ->filter(Input::make())
                ->render(function (RegionTeam $regionTeam) {
                    return $regionTeam->athletes->count();
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (RegionTeam $regionTeam) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.member.athlete.regionTeam.edit', ['regionTeam' => $regionTeam->id ?? 0])
                        ]);
                }),
        ];
    }
}
