<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Athlete;

use App\Models\Activity;
use App\Models\Event\Result;
use App\Models\Member\Arbiter;
use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use App\Models\Organization;
use App\Orchid\Layouts\Member\Athlete\Column\SingleColumn;
use Carbon\Carbon;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class AthleteListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'athletes';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Athlete $athlete) {
                    return $athlete->id;
                }),

            TD::make('first_name', __('ФИО'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Athlete $athlete) {
                    return new SingleColumn($athlete);
                }),

            TD::make('birth_date', __('Дата рождения'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Athlete $athlete) {
                    return Carbon::parse($athlete->birth_date)->format('d.m.Y')  ?? '-';
                }),

            TD::make('gender', __('Пол'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Athlete $athlete) {
                    return $athlete->gender == 'male' ? 'Мужской' : 'Женский';
                }),

            TD::make('region_team_id', __('Собрная'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Athlete $athlete) {
                    return (!is_null($athlete->regionTeam))
                        ? DropDown::make()
                            ->icon('info')
                            ->list([
                                Link::make($athlete->regionTeam->region_name. ' ('. $athlete->regionTeam->age_up_to .' лет)')
                                    ->route('platform.member.athlete.regionTeam.edit', ['regionTeam' => $athlete->regionTeam->id ?? 0])
                                ]
                            )
                        : '';
                }),

            TD::make('organization_id', __('организации'))
                ->cantHide()
                ->render(function (Athlete $athlete) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $athlete->organizations()->count() . '</span> ' .
                        (($athlete->organizations()->count() > 0)
                        ? DropDown::make()
                            ->icon('info')
                            ->list($athlete->organizations->map(fn (Organization $organization) =>
                            Link::make($organization->name. ' ('. ($organization->city ? $organization->city->name : 'город не указан').')')
                                ->route('platform.settings.organization.edit', ['organization' => $organization->id ?? 0])
                            )->toArray())
                        : '');
                }),

            TD::make('coache_id', __('Тренера'))
                ->cantHide()
                ->render(function (Athlete $athlete) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $athlete->coaches()->count() . '</span> ' .
                        (($athlete->coaches()->count() > 0)
                            ? DropDown::make()
                                ->icon('badge')
                                ->list($athlete->coaches->map(fn (Coach $coach) =>
                                Link::make($coach->fio. ' ('. ($coach->rank ?? 'Звание не указано').')')
                                    ->route('platform.member.coach.edit', ['coach' => $coach->id ?? 0])
                                )->toArray())
                            : '');
                }),

            TD::make('activity_id', __('Дисциплины'))
                ->cantHide()
                ->render(function (Athlete $athlete) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $athlete->activities()->count() . '</span> ' .
                        (($athlete->activities()->count() > 0)
                            ? DropDown::make()
                                ->icon('directions')
                                ->list($athlete->activities->map(fn (Activity $activity) =>
                                Link::make($activity->name)
                                    ->route('platform.settings.activities.edit', ['activity' => $activity->id ?? 0])
                                )->toArray())
                            : '');
                }),

            TD::make('results', __('результаты'))
                ->cantHide()
                ->render(function (Athlete $athlete) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $athlete->results()->count() . '</span> ' .
                        (($athlete->results()->count() > 0)
                            ? DropDown::make()
                                ->icon('tag')
                                ->list($athlete->results()->orderByDesc('id')->limit(10)->get()->map(fn (Result $result) =>
                                Link::make($result->activity->name . ' (' . ($result->result_in_meters ?? $result->time). ')')
                                    ->route('platform.event.result.edit', ['result' => $result->id ?? 0])
                                )->toArray())
                            : '');
                }),
        ];
    }
}
