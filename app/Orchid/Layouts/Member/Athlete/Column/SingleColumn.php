<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Athlete\Column;

use App\Models\ActivityClassification;
use App\Models\Member\Arbiter;
use App\Models\Member\Athlete;
use App\Services\Member\AthleteService;
use Illuminate\View\View;
use Orchid\Screen\Layouts\Content;

class SingleColumn extends Content
{
    /**
     * @var string
     */
    protected $template = 'platform::layouts.persona';

    /**
     * @param Athlete $athlete
     *
     * @return View
     */
    public function render(Athlete $athlete): View
    {
        return view($this->template, [
            'title'    => $athlete->fio,
            'subTitle' => 'Разряд : '. ($athlete->qualification ?? 'не указана'),
            'image'    => $athlete->image_url ?? asset('/vendor/default_images/default_user.png'),
            'url'      => route('platform.member.athlete.edit', ['athlete' => $athlete->id ?? 0]),
        ]);
    }
}
