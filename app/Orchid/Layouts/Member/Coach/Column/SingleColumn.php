<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Coach\Column;

use App\Models\ActivityClassification;
use App\Models\Member\Arbiter;
use App\Models\Member\Coach;
use Illuminate\View\View;
use Orchid\Screen\Layouts\Content;

class SingleColumn extends Content
{
    /**
     * @var string
     */
    protected $template = 'platform::layouts.persona';

    /**
     * @param Coach $coach
     *
     * @return View
     */
    public function render(Coach $coach): View
    {
        return view($this->template, [
            'title'    => $coach->fio,
            'subTitle' => 'Ранг : '. ($coach->rank ?? 'не указан'),
            'image'    => $coach->image_url ?? asset('/vendor/default_images/default_user.png'),
            'url'      => route('platform.member.coach.edit', ['coach' => $coach->id ?? 0]),
        ]);
    }
}
