<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Coach;

use App\Models\Member\Coach;
use App\Models\Organization;
use App\Orchid\Layouts\Member\Coach\Column\SingleColumn;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CoachListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'coaches';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Coach $coach) {
                    return $coach->id;
                }),

            TD::make('first_name', __('ФИО'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Coach $coach) {
                    return new SingleColumn($coach);
                }),

            TD::make('email', __('email'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Coach $coach) {
                    return $coach->email ?? '-';
                }),

            TD::make('phone', __('Телефон'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Coach $coach) {
                    return $coach->phone ?? '-';
                }),

            TD::make('organization_id', __('организации'))
                ->cantHide()
                ->render(function (Coach $coach) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $coach->organizations()->count() . '</span> ' .
                        (($coach->organizations()->count() > 0)
                        ? DropDown::make()
                            ->icon('info')
                            ->list($coach->organizations->map(fn (Organization $organization) =>
                            Link::make($organization->name. ' ('. ($organization->city ? $organization->city->name : 'город не указан').')')
                                ->route('platform.settings.organization.edit', ['organization' => $organization->id ?? 0])
                            )->toArray())
                        : '');
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Coach $coach) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.member.coach.edit', ['coach' => $coach->id ?? 0])
                        ]);
                }),
        ];
    }
}
