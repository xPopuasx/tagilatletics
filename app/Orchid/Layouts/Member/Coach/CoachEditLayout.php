<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Coach;

use App\Models\member\Agent;
use App\Models\Member\Coach;
use App\Services\ActivityClassificationCriterionService;
use App\Services\Member\ArbiterCategoryService;
use App\Services\Member\CoachCategoryService;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class CoachEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('coach.last_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Имя судьи'))
                ->placeholder('Укажите имя тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            Input::make('coach.first_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Фамилия судьи'))
                ->placeholder('Укажите фамилиф тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            Input::make('coach.patronymic')
                ->type('text')
                ->title(__('Отчетсво тренера'))
                ->placeholder('Укажите отчество тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            DateTimer::make('coach.birth_date')
                ->format('Y-m-d')
                ->title(__('Дата рождения тренера'))
                ->placeholder('Укажите рождения тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            Input::make('coach.email')
                ->type('text')
                ->title(__('email судьи'))
                ->placeholder('Укажите email тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            Input::make('coach.phone')
                ->type('text')
                ->mask('+7 (999) 999-99-99')
                ->title(__('Телефон тренера'))
                ->placeholder('Укажите телефон тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            Input::make('coach.rank')
                ->type('text')
                ->title(__('Ранг тренера'))
                ->placeholder('Укажите ранг тренера')
                ->help(__('По данному полю вы будите ориентироваться в поиске тренера')),

            Select::make('coach.coach_category_id')
                ->options(array_merge(
                    ['no_value' => 'Выбрать категорию'],
                    (new CoachCategoryService())->getCoachCategoriesToSelect()
                ))
                ->title(__('Выбрать категорию'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске тренера')),

            Select::make('coach.organizations.')
                ->fromModel(Coach::class, 'id')
                ->options((new OrganizationService())->getOrganizationsToSelect())
                ->multiple()
                ->title(__('Выбрать организацию (-и)'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске тренера')),

            Cropper::make('coach.image_url')
                ->groups(config('constants.image_types.coach'))
                ->title('Фотография тренера'),

            Cropper::make('coach.license_image_url')
                ->groups(config('constants.image_types.coach_license'))
                ->title('Лицензия')
        ];
    }
}
