<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Coach\Category;

use App\Models\member\Agent;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class CoachCategoryEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('coachCategory.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Название судейской категории'))
                ->placeholder('Укажите название судейской категории')
                ->help(__('По данному полю вы будите ориентироваться в поиске судейской категории')),
        ];
    }
}
