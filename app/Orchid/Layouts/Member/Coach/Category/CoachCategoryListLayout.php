<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Coach\Category;

use App\Models\Member\ArbiterCategory;
use App\Models\Member\CoachCategory;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CoachCategoryListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'coachCategories';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (CoachCategory $coachCategory) {
                    return $coachCategory->id;
                }),

            TD::make('name', __('Название'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (CoachCategory $coachCategory) {
                    return $coachCategory->name;
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (CoachCategory $coachCategory) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.member.coach.category.edit', ['coachCategory' => $coachCategory->id ?? 0])
                        ]);
                }),
        ];
    }
}
