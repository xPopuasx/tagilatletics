<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Arbiter\Column;

use App\Models\ActivityClassification;
use App\Models\Member\Arbiter;
use Illuminate\View\View;
use Orchid\Screen\Layouts\Content;

class SingleColumn extends Content
{
    /**
     * @var string
     */
    protected $template = 'platform::layouts.persona';

    /**
     * @param Arbiter $arbiter
     *
     * @return View
     */
    public function render(Arbiter $arbiter): View
    {
        return view($this->template, [
            'title'    => $arbiter->fio,
            'subTitle' => 'Приказ : '. ($arbiter->qualification_order ?? 'не указан'),
            'image'    => '',
            'url'      => route('platform.settings.activity.classification.edit', ['activityClassification' => $activityClassification->id ?? 0]),
        ]);
    }
}
