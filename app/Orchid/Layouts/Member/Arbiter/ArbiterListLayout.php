<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Arbiter;

use App\Models\Member\Arbiter;
use App\Models\Organization;
use App\Orchid\Layouts\Member\Arbiter\Column\SingleColumn;
use Carbon\Carbon;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ArbiterListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'arbiters';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Arbiter $arbiter) {
                    return $arbiter->id;
                }),

            TD::make('first_name', __('ФИО'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Arbiter $arbiter) {
                    return new SingleColumn($arbiter);
                }),

            TD::make('email', __('email'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Arbiter $arbiter) {
                    return $arbiter->email ?? '-';
                }),

            TD::make('phone', __('Телефон'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Arbiter $arbiter) {
                    return $arbiter->phone ?? '-';
                }),

            TD::make('date_start', __('Дата от'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Arbiter $arbiter) {
                    return Carbon::parse($arbiter->date_start)->format('d.m.Y') ?? '-';
                }),

            TD::make('date_end', __('Дата до'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Arbiter $arbiter) {
                    return Carbon::parse($arbiter->date_end)->format('d.m.Y')  ?? '-';
                }),

            TD::make('organization_id', __('организации'))
                ->cantHide()
                ->render(function (Arbiter $arbiter) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $arbiter->organizations()->count() . '</span> ' .
                        (($arbiter->organizations()->count() > 0)
                        ? DropDown::make()
                            ->icon('info')
                            ->list($arbiter->organizations->map(fn (Organization $organization) =>
                            Link::make($organization->name. ' ('. ($organization->city ? $organization->city->name : 'город не указан').')')
                                ->route('platform.settings.organization.edit', ['organization' => $organization->id ?? 0])
                            )->toArray())
                        : '');
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Arbiter $arbiter) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.member.arbiter.edit', ['arbiter' => $arbiter->id ?? 0])
                        ]);
                }),
        ];
    }
}
