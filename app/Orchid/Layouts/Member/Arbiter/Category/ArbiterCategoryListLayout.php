<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Arbiter\Category;

use App\Models\Member\ArbiterCategory;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ArbiterCategoryListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'arbiterCategories';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ArbiterCategory $category) {
                    return $category->id;
                }),

            TD::make('name', __('Название'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (ArbiterCategory $category) {
                    return $category->name;
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (ArbiterCategory $category) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.member.arbiter.category.edit', ['arbiterCategory' => $category->id ?? 0])
                        ]);
                }),
        ];
    }
}
