<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Arbiter;

use App\Models\member\Agent;
use App\Models\Member\Arbiter;
use App\Services\ActivityClassificationCriterionService;
use App\Services\Member\ArbiterCategoryService;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class ArbiterEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('arbiter.last_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Имя судьи'))
                ->placeholder('Укажите имя судьи')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            Input::make('arbiter.first_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Фамилия судьи'))
                ->placeholder('Укажите фамилиф судьи')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            Input::make('arbiter.patronymic')
                ->type('text')
                ->title(__('Отчетсво судьи'))
                ->placeholder('Укажите отчество судьи')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            Input::make('arbiter.email')
                ->type('text')
                ->title(__('email судьи'))
                ->placeholder('Укажите email судьи')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            Input::make('arbiter.phone')
                ->type('text')
                ->mask('+7 (999) 999-99-99')
                ->title(__('Телефон судьи'))
                ->placeholder('Укажите телефон судьи')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            Input::make('arbiter.qualification_order')
                ->type('text')
                ->title(__('Приказ о назначении судьи'))
                ->placeholder('Укажите приказ о назначении судьи')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            DateTimer::make('arbiter.date_start')
                ->format('Y-m-d')
                ->title(__('Дата от'))
                ->placeholder('Укажите дату от')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            DateTimer::make('arbiter.date_end')
                ->format('Y-m-d')
                ->title(__('Дата до'))
                ->placeholder('Укажите дату до')
                ->help(__('По данному полю вы будите ориентироваться в поиске судьи')),

            Select::make('arbiter.arbiter_category_id')
                ->options(array_merge(
                    ['no_value' => 'Выбрать категорию'],
                    (new ArbiterCategoryService())->getArbiterCategoriesToSelect()
                ))
                ->title(__('Выбрать критерий'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске классификации')),

            Select::make('arbiter.organizations.')
                ->fromModel(Arbiter::class, 'id')
                ->options((new OrganizationService())->getOrganizationsToSelect())
                ->multiple()
                ->title(__('Выбрать организацию (-и)'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске судьи')),

        ];
    }
}
