<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Agent;

use App\Models\member\Agent;
use App\Services\OrganizationService;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class AgentEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('agent.last_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Имя агента'))
                ->placeholder('Укажите имя агента')
                ->help(__('По данному полю вы будите ориентироваться в поиске агента')),

            Input::make('agent.first_name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Фамилия агента'))
                ->placeholder('Укажите фамилиф агента')
                ->help(__('По данному полю вы будите ориентироваться в поиске агента')),

            Input::make('agent.patronymic')
                ->type('text')
                ->title(__('Отчетсво агента'))
                ->placeholder('Укажите отчество агента')
                ->help(__('По данному полю вы будите ориентироваться в поиске агента')),

            Input::make('agent.email')
                ->type('text')
                ->title(__('email агента'))
                ->placeholder('Укажите email агента')
                ->help(__('По данному полю вы будите ориентироваться в поиске агента')),

            Input::make('agent.phone')
                ->type('text')
                ->mask('+7 (999) 999-99-99')
                ->title(__('Телефон агента'))
                ->placeholder('Укажите телефон агента')
                ->help(__('По данному полю вы будите ориентироваться в поиске агента')),

            Select::make('agent.organizations.')
                ->fromModel(Agent::class, 'id')
                ->options((new OrganizationService())->getOrganizationsToSelect())
                ->multiple()
                ->title(__('Выбрать организацию (-и)'))
                ->help(__('По данному полю вы сможете ориентироваться в поиске анента')),

        ];
    }
}
