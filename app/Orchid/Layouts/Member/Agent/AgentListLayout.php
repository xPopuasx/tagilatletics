<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Member\Agent;

use App\Models\Activity;
use App\Models\member\Agent;
use App\Models\Organization;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class AgentListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'agents';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', __('id'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Agent $agent) {
                    return $agent->id;
                }),

            TD::make('last_name', __('Имя'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Agent $agent) {
                    return $agent->last_name ?? '-';
                }),

            TD::make('first_name', __('Фамилия'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Agent $agent) {
                    return $agent->first_name ?? '-';
                }),

            TD::make('patronymic', __('Отчестов'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Agent $agent) {
                    return $agent->patronymic ?? '-';
                }),

            TD::make('email', __('email'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Agent $agent) {
                    return $agent->email ?? '-';
                }),

            TD::make('phone', __('Телефон'))
                ->sort()
                ->filter(Input::make())
                ->render(function (Agent $agent) {
                    return $agent->phone ?? '-';
                }),

            TD::make('organization_id', __('организации'))
                ->cantHide()
                ->render(function (Agent $agent) {
                    return '<span style="float:left; margin: 5px 10px 0 0;">' . $agent->organizations()->count() . '</span> ' .
                        (($agent->organizations()->count() > 0)
                        ? DropDown::make()
                            ->icon('info')
                            ->list($agent->organizations->map(fn (Organization $organization) =>
                            Link::make($organization->name. ' ('. ($organization->city ? $organization->city->name : 'город не указан').')')
                                ->route('platform.settings.organization.edit', ['organization' => $organization->id ?? 0])
                            )->toArray())
                        : '');
                }),

            TD::make('Actions', 'Действия')
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Agent $agent) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Редактировать'))
                                ->icon('pencil')
                                ->route('platform.member.agent.edit', ['agent' => $agent->id ?? 0])
                        ]);
                }),
        ];
    }
}
