<?php

namespace App\Traits\Model;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait ModelHasOrganizations
{
    public function organizations(): MorphToMany
    {
        return $this->morphToMany(
            Organization::class,
            'organizationable',
            'organizationable',
            'organizationable_id',
            'organization_id'
        );
    }
}
