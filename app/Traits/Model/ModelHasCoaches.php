<?php

namespace App\Traits\Model;

use App\Models\Member\Coach;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait ModelHasCoaches
{
    public function coaches(): MorphToMany
    {
        return $this->morphToMany(
            Coach::class,
            'coachable',
            'coachable',
            'coachable_id',
            'coach_id'
        );
    }
}
