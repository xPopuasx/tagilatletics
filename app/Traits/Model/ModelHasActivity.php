<?php

namespace App\Traits\Model;

use App\Models\Activity;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait ModelHasActivity
{
    public function activities(): MorphToMany
    {
        return $this->morphToMany(
            Activity::class,
            'activityable',
            'activityable',
            'activityable_id',
            'activity_id'
        );
    }
}
