<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'result.athlete_id' => ['required', 'int', Rule::exists('athletes', 'id')],
            'result.coach_id' => ['required', 'int', Rule::exists('coaches', 'id')],
            'result.activity_id' => ['required', 'int', Rule::exists('activities', 'id')],
            'result.event_id' => ['required', 'int', Rule::exists('events', 'id')],
            'result.athlete_place' => ['nullable', 'string'],
            'result.result_hours' => ['nullable', 'string', 'min_digits:2'],
            'result.result_minutes' => ['nullable', 'string', 'min_digits:2'],
            'result.result_seconds' => ['nullable', 'string', 'min_digits:2'],
            'result.result_milliseconds' => ['nullable', 'string', 'min_digits:2'],
            'result.result_in_meters' => ['nullable', 'string'],
            'result.points' => ['nullable', 'int'],
        ];
    }
}
