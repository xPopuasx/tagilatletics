<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'event.name' => ['required', 'string'],
            'event.place_name' => ['required', 'string'],
            'event.date_start' => ['required', 'date', 'date_format:Y-m-d'],
            'event.date_end' => ['required', 'date', 'date_format:Y-m-d'],
            'event.city_id' => ['required', 'integer', Rule::exists('events', 'id')],
        ];
    }
}
