<?php

namespace App\Http\Requests\Settings\City;

use Illuminate\Foundation\Http\FormRequest;

class StoreCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'city.name' => ['required', 'string'],
            'city.region_name' => ['required', 'string'],
            'city.county' => ['required', 'string'],
        ];
    }
}
