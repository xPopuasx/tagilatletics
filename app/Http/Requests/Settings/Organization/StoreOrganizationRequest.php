<?php

namespace App\Http\Requests\Settings\Organization;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'organization.name' => ['required', 'string'],
            'organization.city_id' =>'nullable',
            'organization.phone' => ['nullable', 'string'],
            'organization.site_url' => ['nullable', 'string', 'uri'],
            'organization.image_url' => ['nullable', 'string'],
            'organization.address' => ['nullable', 'string'],
            'organization.director_fio' => ['nullable', 'string'],
            'organization.email' => ['nullable', 'string', 'email']
        ];
    }
}
