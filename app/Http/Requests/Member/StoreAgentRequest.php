<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'agent.first_name' => ['required', 'string'],
            'agent.last_name' => ['required', 'string'],
            'agent.patronymic' => ['nullable', 'string'],
            'agent.email' => ['nullable', 'string', 'email'],
            'agent.phone' => ['nullable', 'string'],
            'agent.organizations' => ['nullable', 'array'],
            'agent.organizations.*' => ['integer', Rule::exists('organizations', 'id')],
        ];
    }
}
