<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAthleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'athlete.first_name' => ['required', 'string'],
            'athlete.last_name' => ['required', 'string'],
            'athlete.patronymic' => ['nullable', 'string'],
            'athlete.birth_date' => ['nullable', 'date', 'date_format:Y-m-d'],
            'athlete.organizations' => ['nullable', 'array'],
            'athlete.organizations.*' => ['integer', Rule::exists('organizations', 'id')],
            'athlete.image_url' => ['nullable', 'string'],
            'athlete.region_team_id' => ['nullable'],
            'athlete.coaches' => ['nullable', 'array'],
            'athlete.coaches.*' => ['integer', Rule::exists('coaches', 'id')],
            'athlete.activities' => ['nullable', 'array'],
            'athlete.activities.*' => ['integer', Rule::exists('activities', 'id')],
            'athlete.insurance' => ['nullable', 'array'],
            'athlete.attachment.*' => ['integer', Rule::exists('attachments', 'id')],
        ];
    }
}
