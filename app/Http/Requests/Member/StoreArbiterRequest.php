<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreArbiterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'arbiter.first_name' => ['required', 'string'],
            'arbiter.last_name' => ['required', 'string'],
            'arbiter.date_start' => ['required', 'date', 'date_format:Y-m-d'],
            'arbiter.date_end' => ['required', 'date', 'date_format:Y-m-d', 'after:date_start'],
            'arbiter.qualification_order' => ['required', 'string'],
            'arbiter.patronymic' => ['nullable', 'string'],
            'arbiter.email' => ['nullable', 'string', 'email'],
            'coach.arbiter_category_id' => ['nullable'],
            'arbiter.phone' => ['nullable', 'string'],
            'arbiter.organizations' => ['nullable', 'array'],
            'arbiter.organizations.*' => ['integer', Rule::exists('organizations', 'id')],
        ];
    }
}
