<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCoachRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'coach.first_name' => ['required', 'string'],
            'coach.last_name' => ['required', 'string'],
            'coach.patronymic' => ['nullable', 'string'],
            'coach.birth_date' => ['nullable', 'date', 'date_format:Y-m-d'],
            'coach.rank' => ['nullable', 'string'],
            'coach.email' => ['nullable', 'string', 'email'],
            'coach.phone' => ['nullable', 'string'],
            'coach.coach_category_id' => ['nullable'],
            'coach.organizations' => ['nullable', 'array'],
            'coach.organizations.*' => ['integer', Rule::exists('organizations', 'id')],
            'coach.image_url' => ['nullable', 'string'],
            'coach.license_image_url' => ['nullable', 'string'],
        ];
    }
}
