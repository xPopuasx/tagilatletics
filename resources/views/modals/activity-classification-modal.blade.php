<div class="bg-white rounded shadow-sm mb-3">
@if(isset($data))
    <table class="table">
        <thead>
        <tr>
            <th colspan="11">{{$data['name']}} (результат спортсмена: {{$data['result']}})</th>
        </tr>
        <tr>
            <th style="width:180px;">Критерий</th>
            <th>МСМК</th>
            <th>МС</th>
            <th>КМС</th>
            <th>I В</th>
            <th>II В</th>
            <th>III В</th>
            <th>I Ю</th>
            <th>II Ю</th>
            <th>III Ю</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data['items'] as $result)
            <tr>
                @foreach($result as $key => $item)
                    <td>
                        <div>{!! $item !!}</div>
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <span style="padding:20px;">Результаты не найдены в системе </span>
@endif
</div>
