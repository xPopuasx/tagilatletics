<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('athletes', function (Blueprint $table) {
            $table->id();
            $table->string('last_name')->comment('Фамилия');
            $table->string('first_name')->comment('Имя');
            $table->string('patronymic')->nullable()->comment('Отчестов');
            $table->date('birth_date')->nullable()->comment('дата рождения');
            $table->boolean('umo')->default(0)->comment('дата рождения');
            $table->string('image_url')->nullable()->comment('Изображение атлета');
            $table->string('qualification')->nullable()->comment('Изображение лицензии');
            $table->unsignedBigInteger('region_team_id')->nullable();
            $table->foreign('region_team_id')->references('id')->on('region_teams')->nullOnDelete()->nullOnDelete();
            $table->enum('gender', ['male', 'female'])->nullable()->comment('дата рождения');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('athletes');
    }
};
