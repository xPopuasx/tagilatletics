<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_classifications', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Название классификации');
            $table->string('unit')->nullable()->comment('единица измерения');
            $table->unsignedBigInteger('criterion_id')->nullable();
            $table->foreign('criterion_id')->references('id')->on('activity_classification_criteria')->nullOnDelete();
            $table->string('discharge_msmk')->nullable()->comment('Разряд мастер спорта международного класса');
            $table->string('discharge_ms')->nullable()->comment('Разряд мастер спорта');
            $table->string('discharge_kms')->nullable()->comment('Разряд кандидата в мастер спорта');
            $table->string('discharge_first_adult')->nullable()->comment('Разряд 1 взрослый');
            $table->string('discharge_second_adult')->nullable()->comment('Разряд 2 взрослый');
            $table->string('discharge_third_adult')->nullable()->comment('Разряд 3 взрослый');;
            $table->string('discharge_first_youthful')->nullable()->comment('Разряд 1 юношаский');
            $table->string('discharge_second_youthful')->nullable()->comment('Разряд 2 юношаский');
            $table->string('discharge_third_youthful')->nullable()->comment('Разряд 3 юношаский');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_classifications');
    }
};
