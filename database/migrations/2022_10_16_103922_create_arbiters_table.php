<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arbiters', function (Blueprint $table) {
            $table->id();
            $table->string('last_name')->comment('Фамилия');
            $table->string('first_name')->comment('Имя');
            $table->string('patronymic')->nullable()->comment('Отчестов');
            $table->string('email')->nullable()->comment('Емаил');
            $table->string('phone')->nullable()->comment('Телефон');
            $table->date('date_start')->nullable()->comment('Дата начала');
            $table->string('date_end')->nullable()->comment('Дата окончания');
            $table->string('qualification_order')->nullable()->comment('Приказ о квалификации');
            $table->unsignedBigInteger('arbiter_category_id')->nullable();
            $table->foreign('arbiter_category_id')->references('id')->on('arbiter_categories')->nullOnDelete()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arbiters');
    }
};
