<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coaches', function (Blueprint $table) {
            $table->id();
            $table->string('last_name')->comment('Фамилия');
            $table->string('first_name')->comment('Имя');
            $table->string('patronymic')->nullable()->comment('Отчестов');
            $table->string('email')->nullable()->comment('Емаил');
            $table->date('birth_date')->nullable()->comment('дата рождения');
            $table->string('phone')->nullable()->comment('Телефон');
            $table->string('rank')->nullable()->comment('Ранг тренера');
            $table->string('image_url')->nullable()->comment('Изображение тренра');
            $table->string('license_image_url')->nullable()->comment('Изображение лицензии');
            $table->unsignedBigInteger('coach_category_id')->nullable();
            $table->foreign('coach_category_id')->references('id')->on('coach_categories')->nullOnDelete()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coaches');
    }
};
