<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('athlete_id')->nullable();
            $table->foreign('athlete_id')->references('id')->on('athletes')->nullOnDelete();
            $table->unsignedBigInteger('coach_id')->nullable();
            $table->foreign('coach_id')->references('id')->on('coaches')->nullOnDelete();
            $table->unsignedBigInteger('activity_id')->nullable();
            $table->foreign('activity_id')->references('id')->on('activities')->nullOnDelete();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('events')->nullOnDelete();
            $table->integer('athlete_place')->nullable()->comment('Занятое место');
            $table->string('result_hours')->nullable()->comment('Результат часов');
            $table->string('result_minutes')->nullable()->comment('Результат минут');
            $table->string('result_seconds')->nullable()->comment('Результат секунд');
            $table->string('result_milliseconds')->nullable()->comment('Результат милисекунд');
            $table->bigInteger('result_time_in_milliseconds')->nullable()->comment('Временной результат в милисекундах');
            $table->string('result_in_meters')->nullable()->comment('Результат метрах');
            $table->integer('points')->nullable()->comment('Очки');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
};
