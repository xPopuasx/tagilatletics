<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Название организации');
            $table->string('phone')->nullable()->comment('Телефон организации');
            $table->string('email')->nullable()->comment('Почта организации');
            $table->string('site_address')->nullable()->comment('Сайт организации');
            $table->string('address')->nullable()->comment('Адрес организации');
            $table->string('image_url')->nullable()->comment('Изображение организации');
            $table->string('director_fio')->nullable()->comment('ФИО директора');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
};
