<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\City;
use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use App\Models\Member\RegionTeam;
use App\Models\Organization;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RegionTeamSeeder extends Seeder
{
    public function run(): void
    {
        $cities = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')
            ->whereNotNull('sbso')
            ->select('sbso as age_up_to')
            ->groupBy('age_up_to')
            ->get();

        $this->insertToTable(RegionTeam::class, $cities);
    }



    private function insertToTable(string $model, Collection $data){
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }
}
