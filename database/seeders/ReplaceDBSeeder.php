<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\ActivityClassification;
use App\Models\ActivityClassificationCriterion;
use App\Models\Member\Arbiter;
use App\Models\City;
use App\Models\member\Agent;
use App\Models\Member\ArbiterCategory;
use App\Models\Organization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ReplaceDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Города
        $cities = DB::connection(config('constants.seeds.dumb_db_name'))->table('sity')
            ->select('sity_name as name', 'reg_name as region_name', 'okrug as county')
            ->get();

        $this->insertToTable(City::class, $cities);

        // Дисциплины
        $activities = DB::connection(config('constants.seeds.dumb_db_name'))->table('activities')
            ->select('dist_name as name')
            ->get();

        $this->insertToTable(Activity::class, $activities);

        // Критерии классификаций
        $activityClassificationCriterion = DB::connection(config('constants.seeds.dumb_db_name'))->table('evsk')
            ->whereNotNull('criterion')
            ->select(
                'criterion as name'
            )
            ->groupBy('name')
            ->get();

        $this->insertToTable(ActivityClassificationCriterion::class, $activityClassificationCriterion);

        // Классификации
        $activitiesClassifications = DB::connection(config('constants.seeds.dumb_db_name'))->table('evsk')
            ->select(
                'discipline as name',
                'criterion as criterion_id',
                'unit as unit',
                'msmk as discharge_msmk',
                'ms as discharge_ms',
                'kms as discharge_kms',
                '1sp as discharge_first_adult',
                '2sp as discharge_second_adult',
                '3sp as discharge_third_adult',
                '1jun as discharge_first_youthful',
                '2jun as discharge_second_youthful',
                '3jun as discharge_third_youthful')
            ->get();

        foreach($activitiesClassifications as &$activitiesClassification){
            $activitiesClassification->name = str_replace('  ',' ',$activitiesClassification->name);
            $activitiesClassification->criterion_id = ActivityClassificationCriterion::query()
                ->select('id')->where('name', $activitiesClassification->criterion_id)->first()->id ?? null;
        }

        $this->insertToTable(ActivityClassification::class, $activitiesClassifications);

        // Организации

        $agents = DB::connection(config('constants.seeds.dumb_db_name'))->table('agent')
            ->select(
                'last_name',
                'first_name',
                'patronymic',
                'tel as phone',
                'mail as email',
                'org'
            )
            ->get()->map(function($item){
                return [
                    'data' => [
                        'last_name' => $item->last_name,
                        'first_name' => $item->last_name,
                        'patronymic' => $item->patronymic,
                        'phone' => $item->phone,
                        'email' => $item->email,
                    ],
                    'relations' => [
                        'organizations' => [
                                Organization::query()
                                    ->select('id')->whereIn('name', [$item->org])->first()->id ?? null
                        ]
                    ]
                ];
            })->toArray();

        $this->insertToTableWithMorphRelate(Agent::class, $agents);

        $arbiterCategories = DB::connection(config('constants.seeds.dumb_db_name'))->table('arbiter')
            ->whereNotNull('cat')
            ->select(
                'cat as name'
            )
            ->groupBy('name')
            ->get();

        $this->insertToTable(ArbiterCategory::class, $arbiterCategories);

        $arbiters = DB::connection(config('constants.seeds.dumb_db_name'))->table('arbiter')
            ->select(
                'last_name',
                'first_name',
                'patronymic',
                'phone',
                'mail as email',
                'date as date_start',
                'date_end as date_end',
                'num as qualification_order',
                'cat',
                'org'
            )
            ->get()->map(function($item){
                return [
                    'data' => [
                        'last_name' => $item->last_name,
                        'first_name' => $item->last_name,
                        'patronymic' => $item->patronymic,
                        'phone' => $item->phone,
                        'email' => $item->email,
                        'date_start' => $item->date_start,
                        'date_end' => $item->date_end,
                        'qualification_order' => $item->qualification_order,
                        'arbiter_category_id' => ArbiterCategory::query()
                                ->select('id')->where('name', $item->cat)->first()->id ?? null,
                    ],
                    'relations' => [
                        'organizations' => [
                                Organization::query()
                                    ->select('id')->whereIn('name', [$item->org])->first()->id ?? null
                        ]
                    ]
                ];
            })->toArray();


        $this->insertToTableWithMorphRelate(Arbiter::class, $arbiters);
    }

    private function insertToTable(string $model, Collection $data){
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }

    private function insertToTableWithMorphRelate(string $model, array $data)
    {
        foreach ($data as $item){
            $column = (new $model)->query()->create($item['data']);

            foreach ($item['relations'] as $key => $relation){
                $relation = array_filter($relation, 'strlen');

                if(count($relation) > 0){
                    $column->{$key}()->sync(array_values($relation));
                }
            }
        }
    }

}
