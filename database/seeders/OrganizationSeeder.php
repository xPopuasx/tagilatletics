<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\City;
use App\Models\Organization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrganizationSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $organizations_agents = DB::connection(config('constants.seeds.dumb_db_name'))->table('agent')->whereNotNull('org')
            ->select('org as name', 'sity as city_id');

        $organizations_arbiter = DB::connection(config('constants.seeds.dumb_db_name'))->table('arbiter')->whereNotNull('org')
            ->select('org as name', 'sity as city_id');

        $organizations_athlets = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')->whereNotNull('org1')
            ->select('org1 as name', 'sity as city_id');
        $organizations_athlets_2 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')->whereNotNull('org1')
            ->select('org2 as name', 'sity as city_id');
        $organizations_athlets_3 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')->whereNotNull('org1')
            ->select('org3 as name', 'sity as city_id');
        $organizations_athlets_4 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')->whereNotNull('org1')
            ->select('org4 as name', 'sity as city_id');
        $organizations_athlets_5 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')->whereNotNull('org1')
            ->select('org5 as name', 'sity as city_id');
        $organizations_athlets_6 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')->whereNotNull('org1')
            ->select('org6 as name', 'sity as city_id');

        $organizations_athlets_tumen = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets_tumen')->whereNotNull('org1')
            ->select('org1 as name', 'sity as city_id');
        $organizations_athlets_tumen_2 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets_tumen')->whereNotNull('org1')
            ->select('org5 as name', 'sity as city_id');
        $organizations_athlets_tumen_3 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets_tumen')->whereNotNull('org1')
            ->select('org1 as name', 'sity as city_id');
        $organizations_athlets_tumen_4 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets_tumen')->whereNotNull('org1')
            ->select('org4 as name', 'sity as city_id');
        $organizations_athlets_tumen_5 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets_tumen')->whereNotNull('org1')
            ->select('org5 as name', 'sity as city_id');
        $organizations_athlets_tumen_6 = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets_tumen')->whereNotNull('org1')
            ->select('org6 as name', 'sity as city_id');

        $organizations_coach = DB::connection(config('constants.seeds.dumb_db_name'))->table('coach')->whereNotNull('org')
            ->select('org as name', 'sity as city_id');
        $organizations_coach_2 = DB::connection(config('constants.seeds.dumb_db_name'))->table('coach')->whereNotNull('org')
            ->select('org2 as name', 'sity as city_id');
        $organizations_coach_3 = DB::connection(config('constants.seeds.dumb_db_name'))->table('coach')->whereNotNull('org')
            ->select('org3 as name', 'sity as city_id');

        $organizations = $organizations_agents
            ->union($organizations_arbiter)
            ->union($organizations_athlets)
            ->union($organizations_athlets_2)
            ->union($organizations_athlets_3)
            ->union($organizations_athlets_4)
            ->union($organizations_athlets_5)
            ->union($organizations_athlets_6)
            ->union($organizations_athlets_tumen)
            ->union($organizations_athlets_tumen_2)
            ->union($organizations_athlets_tumen_3)
            ->union($organizations_athlets_tumen_4)
            ->union($organizations_athlets_tumen_5)
            ->union($organizations_athlets_tumen_6)
            ->union($organizations_coach)
            ->union($organizations_coach_2)
            ->union($organizations_coach_3)
            ->get();

        foreach($organizations as $key => &$organization){
            $organization->city_id = City::query()
                ->select('id')->where('name', $organization->city_id)->first()->id ?? null;
            $organization->created_at = now();
            $organization->updated_at = now();

            if(strlen($organization->name) == 0){
                $organizations->forget($key);
            }
        }

        $this->insertToTable(Organization::class, $organizations->values());

        $organizations = DB::connection(config('constants.seeds.dumb_db_name'))->table('organization')->whereNotNull('org_name')
            ->select(
                'org_name as name',
                'director as director_fio',
                'tel as phone',
                'mail as email',
                'site as site_address',
                'address',
                'image as image_url',
                'org_sity as city_id'
            )->get();

        foreach($organizations as $key => &$organization){
            $organization->city_id = City::query()
                ->select('id')->where('name', $organization->city_id)->first()->id ?? null;
            $organization->created_at = now();
            $organization->updated_at = now();

            if(strlen($organization->name) == 0){
                $organizations->forget($key);
            }
        }

        $this->updateOrCreateOrganizations(Organization::class, $organizations->values());
    }

    private function insertToTable(string $model, Collection $data): void
    {
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }


    private function updateOrCreateOrganizations(string $model, Collection $data): void
    {
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){

            foreach($item as $organization){
                (new $model)->query()->updateOrCreate(['name' => $organization['name']], $organization);
            }
        }
    }
}
