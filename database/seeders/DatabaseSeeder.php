<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OrganizationSeeder::class,
            CoachCategorySeeder::class,
            CoachSeeder::class,
            ReplaceDBSeeder::class,
            EventSeeder::class,
            RegionTeamSeeder::class,
            AthleteSeeder::class,
            ResultsSeeder::class,
        ]);
    }
}
