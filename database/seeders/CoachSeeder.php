<?php

namespace Database\Seeders;

use App\Models\Member\Coach;
use App\Models\Member\CoachCategory;
use App\Models\Organization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CoachSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $coachAthletes = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')
            ->select(
                'trID1',
                'trID2',
                'trID3',
                'trID4',
                'trID5',
                'trID6',
            )->get()->map(function ($item){
                $coaches = [];

                if(strlen($item->trID1) > 0){
                    $coaches[1]  = explode(' ', $item->trID1);
                }

                if(strlen($item->trID2) > 0){
                    $coaches[2]  = explode(' ', $item->trID2);
                }

                if(strlen($item->trID3) > 0){
                    $coaches[3]  = explode(' ', $item->trID3);
                }

                if(strlen($item->trID4) > 0){
                    $coaches[4]  = explode(' ', $item->trID4);
                }

                if(strlen($item->trID5) > 0){
                    $coaches[5]  = explode(' ', $item->trID5);
                }

                if(strlen($item->trID6) > 0){
                    $coaches[6]  = explode(' ', $item->trID6);
                }

                $data = [];

                foreach($coaches as $coach){
                    if( isset($coach[0]) && isset($coach[1]) ){
                        $data = [
                            'last_name' => $coach[0],
                            'first_name' => $coach[1],
                            'patronymic' => $coach[2] ?? null,
                            'phone' => null,
                            'email' => null,
                            'rank' => null,
                            'birth_date' =>null,
                            'image_url' => null,
                            'license_image_url' => null,
                            'coach_category_id' => null,
                        ];
                    }
                }

                return $data;
            });

        $data = [];

        foreach($coachAthletes as $coachAthleteValue){
            if(isset($coachAthleteValue['last_name'])){
                $data[] = $coachAthleteValue;
            }
        }

        $data = collect($data)->unique()->values();

        $this->insertToTable(Coach::class, $data);

        $coaches = DB::connection(config('constants.seeds.dumb_db_name'))->table('coach')
            ->select(
                'last_name',
                'first_name',
                'patronymic',
                'birth as birth_date',
                'category',
                'rank',
                'phone',
                'mail as email',
                'image as image_url',
                'license as license_image_url',
                'org',
                'org2',
                'org3',
            )
            ->get()->map(function($item){
                return [
                    'data' => [
                        'last_name' => $item->last_name,
                        'first_name' => $item->last_name,
                        'patronymic' => $item->patronymic,
                        'phone' => $item->phone,
                        'email' => $item->email,
                        'rank' => $item->rank,
                        'birth_date' => $item->birth_date,
                        'image_url' => $item->image_url,
                        'license_image_url' => $item->license_image_url,
                        'coach_category_id' => CoachCategory::query()
                                ->select('id')->where('name', $item->category)->first()->id ?? null,
                    ],
                    'relations' => [
                        'organizations' => [
                                Organization::query()
                                    ->select('id')->where('name', $item->org)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org2)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org3)->first()->id ?? null
                        ]
                    ]
                ];
            })->toArray();


        $this->insertToTableWithMorphRelate(Coach::class, $coaches);

    }

    private function insertToTable(string $model, Collection $data){
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }

    private function insertToTableWithMorphRelate(string $model, array $data)
    {
        foreach ($data as $item){
            $column = (new $model)->query()->updateOrCreate([
                'last_name' => $item['data']['last_name'],
                'first_name' => $item['data']['first_name'],
                'patronymic' => $item['data']['patronymic']
            ], $item['data']);

            foreach ($item['relations'] as $key => $relation){
                $relation = array_filter($relation, 'strlen');

                if(count($relation) > 0){
                    $column->{$key}()->sync(array_values($relation));
                }
            }
        }
    }
}
