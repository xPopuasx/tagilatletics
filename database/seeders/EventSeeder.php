<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Event\Event;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    public function run(): void
    {
        $events = DB::connection(config('constants.seeds.dumb_db_name'))->table('competitions')
            ->select('name','firstDate as date_start', 'lastDate as date_end', 'city as city_id', 'place as place_name')
            ->get();

        foreach($events as &$event){
            $event->city_id = City::query()
                ->select('id')->where('name', $event->city_id)->first()->id ?? null;
            $event->created_at = now();
            $event->updated_at = now();
        }

        $this->insertToTable(Event::class, $events);
    }



    private function insertToTable(string $model, Collection $data){
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }
}
