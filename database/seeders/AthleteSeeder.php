<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use App\Models\Member\RegionTeam;
use App\Models\Organization;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AthleteSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $coaches = DB::connection(config('constants.seeds.dumb_db_name'))->table('athlets')
            ->select(
                'athlets.last_name',
                'athlets.first_name',
                'athlets.patronymic',
                'athlets.bird as birth_date',
                'athlets.qualify as qualification',
                DB::raw('(CASE
                    WHEN athlets.sex = "Ж" THEN "female"
                    WHEN athlets.sex = "М" THEN "male"
                END) as gender'),
                'athlets.umo',
                'athlets.image as image_url',
                'athlets.sbso',
                'athlets.trID1',
                'athlets.trID2',
                'athlets.trID5',
                'athlets.trID3',
                'athlets.trID4',
                'athlets.trID5',
                'athlets.trID6',
                'athlets.event',
                'athlets.event1',
                'athlets.org1',
                'athlets.org2',
                'athlets.org3',
                'athlets.org4',
                'athlets.org5',
                'athlets.org6',
                'athlets.org1',
                'athlets.coach1',
                'athlets.coach2',
                'athlets.coach3',
                'athlets.coach4',
                'ch1.first_name as coach1_first_name',
                'ch1.last_name as coach1_last_name',
                'ch1.patronymic as coach1_patronymic',
                'ch2.first_name as coach2_first_name',
                'ch2.last_name as coach2_last_name',
                'ch2.patronymic as coach2_patronymic',
                'ch3.first_name as coach3_first_name',
                'ch3.last_name as coach3_last_name',
                'ch3.patronymic as coach3_patronymic',
                'ch4.first_name as coach4_first_name',
                'ch4.last_name as coach4_last_name',
                'ch4.patronymic as coach4_patronymic',
            )->leftJoin('coach as ch1', 'ch1.id', '=', 'athlets.coach1')
            ->leftJoin('coach as ch2', 'ch2.id', '=', 'athlets.coach2')
            ->leftJoin('coach as ch3', 'ch3.id', '=', 'athlets.coach3')
            ->leftJoin('coach as ch4', 'ch4.id', '=', 'athlets.coach4')
            ->get()->map(function($item){
                return [
                    'data' => [
                        'last_name' => $item->last_name,
                        'first_name' => $item->first_name,
                        'patronymic' => $item->patronymic,
                        'birth_date' => $item->birth_date,
                        'qualification' => $item->qualification,
                        'gender' => $item->gender,
                        'umo' => $item->umo ?? 0,
                        'image_url' => $item->image_url,
                        'region_team_id' => RegionTeam::query()
                                ->select('id')->where('age_up_to', $item->sbso)->first()->id ?? null,
                    ],
                    'relations' => [
                        'organizations' => [
                                Organization::query()
                                    ->select('id')->where('name', $item->org1)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org2)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org3)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org4)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org5)->first()->id ?? null,
                                Organization::query()
                                    ->select('id')->where('name', $item->org6)->first()->id ?? null,
                        ],
                        'coaches' => [
                                Coach::query()
                                    ->select('id')->where('last_name', explode(' ', $item->trID1)[0] ?? null)
                                    ->where('first_name', explode(' ', $item->trID1)[1] ?? null)
                                    ->where('patronymic', explode(' ', $item->trID1)[2] ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', explode(' ', $item->trID2)[0] ?? null)
                                    ->where('first_name', explode(' ', $item->trID2)[1] ?? null)
                                    ->where('patronymic', explode(' ', $item->trID2)[2] ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', explode(' ', $item->trID3)[0] ?? null)
                                    ->where('first_name', explode(' ', $item->trID3)[1] ?? null)
                                    ->where('patronymic', explode(' ', $item->trID3)[2] ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', explode(' ', $item->trID4)[0] ?? null)
                                    ->where('first_name', explode(' ', $item->trID4)[1] ?? null)
                                    ->where('patronymic', explode(' ', $item->trID4)[2] ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', explode(' ', $item->trID5)[0] ?? null)
                                    ->where('first_name', explode(' ', $item->trID5)[1] ?? null)
                                    ->where('patronymic', explode(' ', $item->trID5)[2] ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', explode(' ', $item->trID6)[0] ?? null)
                                    ->where('first_name', explode(' ', $item->trID6)[1] ?? null)
                                    ->where('patronymic', explode(' ', $item->trID6)[2] ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', $item->coach1_first_name)
                                    ->where('first_name', $item->coach1_last_name)
                                    ->where('patronymic', $item->coach1_patronymic ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', $item->coach2_first_name)
                                    ->where('first_name', $item->coach2_last_name)
                                    ->where('patronymic', $item->coach2_patronymic ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', $item->coach3_first_name)
                                    ->where('first_name', $item->coach3_last_name)
                                    ->where('patronymic', $item->coach3_patronymic ?? null)->first()->id ?? null,
                                Coach::query()
                                    ->select('id')->where('last_name', $item->coach4_first_name)
                                    ->where('first_name', $item->coach4_last_name)
                                    ->where('patronymic', $item->coach4_patronymic ?? null)->first()->id ?? null
                            ],
                        'activities' => [
                                Activity::query()
                                    ->select('id')->where('name', str_replace('метров','',$item->event).'м')->first()->id ?? null,
                                Activity::query()
                                    ->select('id')->where('name',  str_replace('метров','',$item->event1).'м')->first()->id ?? null,
                        ]
                    ]
                ];
            })->toArray();

        $this->insertToTableWithMorphRelate(Athlete::class, $coaches);

    }

    private function insertToTableWithMorphRelate(string $model, array $data)
    {
        foreach ($data as $item){
            $column = (new $model)->query()->updateOrCreate([
                'last_name' => $item['data']['last_name'],
                'first_name' => $item['data']['first_name'],
                'patronymic' => $item['data']['patronymic']
            ], $item['data']);

            foreach ($item['relations'] as $key => $relation){
                $relation = array_filter($relation, 'strlen');

                if(count($relation) > 0){
                    $column->{$key}()->sync(array_values($relation));
                }
            }
        }
    }
}
