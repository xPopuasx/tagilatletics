<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Activity;
use App\Models\Event\Event;
use App\Models\Event\Result;
use App\Models\Member\Athlete;
use App\Models\Member\Coach;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ResultsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $results = DB::connection(config('constants.seeds.dumb_db_name'))->table('results')
            ->select(
                'results.competition',
                'results.distance',
                'results.result',
                'results.place',
                'results.points',
                'results.time',
                'results.coach',
                'results.competition',
                'results.id',
                'ath.last_name as ath_last_name',
                'ath.first_name as ath_first_name',
                'ath.patronymic as ath_patronymic',
                'ch.last_name as ch_last_name',
                'ch.first_name as ch_first_name',
                'ch.patronymic as ch_patronymic',
                'act.dist_name as act_dist_name',
                'cmpt.name as event_name'
            )
            ->leftJoin('coach as ch', 'ch.id', '=', 'results.coach')
            ->leftJoin('athlets as ath', 'ath.id', '=', 'results.id')
            ->leftJoin('activities as act', 'act.id', '=', 'results.distance')
            ->leftJoin('competitions as cmpt', 'cmpt.id', '=', 'results.competition')
            ->get();

        foreach($results as &$result){
            if(!str_contains($result->result, '.') && strlen($result->result) <= 6){
                $result->result = $result->result.'.0';
            }

            $result->result_in_meters =  null;

            if(preg_match('/\d/', $result->act_dist_name) || $result->act_dist_name == 'Трейл'){
                $resultTime = Carbon::parse($result->result);

                $hours = $resultTime->hour;
                if(($hours != 0 && strlen($hours) == 1) || $hours == 0){
                    $hours = '0'.$resultTime->hour;
                }

                $minutes = $resultTime->minute;
                if(($minutes != 0 && strlen($minutes) == 1) || $minutes == 0){
                    $minutes = '0'.$resultTime->minute;
                }

                $seconds = $resultTime->second;
                if(($seconds != 0 && strlen($seconds) == 1) || $seconds == 0){
                    $seconds = '0'.$resultTime->second;
                }

                $milliseconds = $resultTime->millisecond / 10;
                if(($milliseconds != 0 && strlen($milliseconds) == 1) || $milliseconds == 0){
                    $milliseconds = '0'.$resultTime->millisecond;
                }
            } else {
                $result->result_in_meters = $result->result;
            }


            $result->result_hours = (!$result->result_in_meters) ? $hours : null;
            $result->result_minutes = (!$result->result_in_meters) ? $minutes : null;
            $result->result_seconds = (!$result->result_in_meters) ? $seconds : null;
            $result->result_milliseconds = (!$result->result_in_meters) ? $milliseconds : null;
            $result->result_time_in_milliseconds = (!$result->result_in_meters) ? $resultTime->getPreciseTimestamp(3) : null;

            $result->athlete_id = Athlete::query()
                ->select('id')
                ->where('last_name', $result->ath_last_name)
                ->where('first_name', $result->ath_first_name)
                ->where('patronymic', $result->ath_patronymic)
                ->first()->id ?? null;

            $result->event_id = Event::query()
                ->where('name', $result->event_name)
                ->first()->id ?? null;

            $result->coach_id = Coach::query()
                ->select('id')
                ->where('last_name', $result->ch_last_name)
                ->where('first_name', $result->ch_first_name)
                ->where('patronymic', $result->ch_patronymic)
                ->first()->id ?? null;

            $result->activity_id = Activity::query()
                ->select('id')->where('name', $result->act_dist_name)->first()->id ?? null;
            $result->created_at = now();
            $result->updated_at = now();
        }

        $this->insertToTable(Result::class, $results);
    }

    private function insertToTable(string $model, Collection $data): void
    {
        $data = array_map(function($item) {
            return [
                'result_hours' => $item->result_hours,
                'result_minutes' => $item->result_minutes,
                'result_seconds' => $item->result_seconds,
                'result_milliseconds' => $item->result_milliseconds,
                'result_time_in_milliseconds' => $item->result_time_in_milliseconds,
                'activity_id' => $item->activity_id,
                'coach_id' => $item->coach_id,
                'event_id' => $item->event_id,
                'athlete_id' => $item->athlete_id,
                'result_in_meters' => $item->result_in_meters,
                'athlete_place' => str_contains($item->place, 'место') ? str_replace(' место','',$item->place) : 0,
                'points' => $item->points,
            ];
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }
}
