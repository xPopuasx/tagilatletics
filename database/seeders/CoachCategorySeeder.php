<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\City;
use App\Models\Member\ArbiterCategory;
use App\Models\Member\CoachCategory;
use App\Models\Organization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CoachCategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $coachCategory = DB::connection(config('constants.seeds.dumb_db_name'))->table('coach')
            ->whereNotNull('category')
            ->select(
                'category as name'
            )
            ->groupBy('name')
            ->get();

        $this->insertToTable(CoachCategory::class, $coachCategory);
    }

    private function insertToTable(string $model, Collection $data): void
    {
        $data = array_map(function($item) {
            return (array)$item;
        }, $data->toArray());

        $data = array_chunk($data, 200);

        foreach ($data as $item){
            (new $model)->query()->insert($item);
        }
    }
}
